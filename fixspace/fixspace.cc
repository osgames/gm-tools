// fixspace.cc - fix whitespace
// replace tabs with spaces and remove trailing whitespace

#include <iostream> // stream operations
#include <fstream>  // file io

const int TAB = 6; // number of spaces to a tab

// error values
const int errNONE = 0;
const int errFILEIO = 1;
const int errUSEAGE = 2;


using namespace std;

int main(int argc, char** argv)
{
  char c; // input character
  int space = 0; // number of spaces
  int i; // counter
   
  if (argc != 3)
  {
    cout << "Useage: fixspace <infilename> <outfilename>" << endl;
    exit(errUSEAGE);
  }//fi useage
  // open input file
  ifstream in(argv[1], ios::in);
  if (!in) 
  {
    cout << "failed to open input file: " << argv[1] << endl;
    exit(errFILEIO);
  } // open input file
  
  // open output file
  ofstream out(argv[2], ios::out);
  if (!out) 
  {
    cout << "failed to open output file: " << argv[2] << endl;
    exit(errFILEIO);
  } // open input file
  
  // process input to output  
  
  while (in)
  {
    //in >> c;
    in.get(c);
    if (c == ' ') 
    { 
      space++;
    } else if (c == '\t') 
      space += TAB;
    else 
    {
      // ignore trailing whitespace
      if (c == '\n' || c == '\r') space = 0;
      
      // print normal whitespace
      for (i = 0; i < space; i++) out << ' ';
      space = 0; // clear counter
      
      out << c;
    } // if else cascade
  } // while
  
  
  
  // close file handles
  in.close();
  out.close();
  
  return errNONE;
}//main
