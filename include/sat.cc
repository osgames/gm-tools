/*  sat.cc -- Strings and Things: Formatting of Strings
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

//   
#include <stdio.h>  // required for sprintf - perhaps there is another way ?
//#include <curses.h>
#include <string.h> 
#include <stdlib.h>

#include "sat.h"    
   		    
char * set_field(char* to_eddit, int field_width)
// creates a string of <field_width> characters containing <to_eddit>
// if to_eddit is longer than <field_width> chars then it will be truncated
// calling function MUST delete returned string
{  		    
  char * new_field; 
   		    
  new_field = new_str(field_width);
  strncpy(new_field, to_eddit, strlen(to_eddit));
  return new_field; 
}// set field	    
   		    
char * set_field(int to_eddit, int field_width)
// creates a string of <field_width> characters containing <to_eddit>
// if to_eddit is longer than <field_width> then it will be truncated
// calling function MUST delete returned string
{  		    
  char * new_field; 
   		    
  new_field = new_str(field_width);
  sprintf(new_field, "%d", to_eddit);
  new_field[(strlen(new_field))] = ' ';
   		    
  new_field[field_width-1] = '\n'; // should allready be the case bu be sure
   		    
  return new_field; 
}// set field	    
   		    
char* new_prn_pts(int pts)
// convers pts (in xp) to a printable (char*) character points
   	     
{  	     
  char * buff = new char[40];
  if (!(pts < 0))
    sprintf(buff, "%d.%d", pts/10, pts%10);
  else	     
    sprintf(buff, "-%d.%d", -pts/10, -pts%10);
  return buff;
}// statistic:: print points
   	     
   	     
void del_chars( char* s, int nchars)
{  	     
// delete nchars from s
// insert nchars on right
   	     
// nchars must be <= strlen(s)
// nchars defults to 1
   	     
  char* p2 = s + nchars;  // character after last to delete
  while (*p2 != '\0')     // until all are moved
    *s++ = *p2++;         // move kept characters back
	     
  while (*s != '\0')      // until the end of the string
    *s++ = ' ';           // add blanks
} // del chars
	     
void ins_chars( char* s, int nchars )
{	     
// insert nchars of spaces on left
// drop nchars from right
	     
// nchars must be <= strlen(s)
// nchars defults to 1
	     
  char* p2 = s + strlen(s) - 1; // point to last char
  char* p1 = p2 - nchars;
	     
  while ( p1 >= s)
    *p2-- = *p1--; // copy existing chars to the right
	     
  while ( p2 > p1 )
    *p2-- = ' ';  //  fill hole with spaces
} // ins chars
	     
char *new_str( int nchars )
// allocates a new string of nchars blanks
{     	     		    
  char *tmp = (char *) NULL;
  int count; 	       	    
    	     	       	    
  if (nchars < 1) return tmp;
  tmp = new char[(nchars+1)];
	 	       
  count = 0;	       
  while (count < nchars)
  {	 	       
    tmp[count] = ' ';  
    count++;	       
  }// while	       
  tmp[nchars] = '\0';  
	 	       
  return tmp;	       
}// new_str (int)      
    	 	       
char* new_str( const char* str )
// allocates a copy of str
{   
  char *tmp;
  int len;
    
  if (str == NULL) return (char *) NULL;
  len = strlen(str) + 1;   	
  if (len == 1) return (char *) NULL;
  if ((tmp = new char[len]) == NULL) exit(EXIT_FAILURE);
  memcpy(tmp, str, len);

  return tmp;
}// new_str (char*)
    
char* right_word( char* here )
/* find the left end of the word on the right word */
{   
// exit current word, if any
  while (isalnum(*here) || (*here == ')'))
    here++;
// pass overwhite space
  while (
         !(
              isalnum(*here)
           || (*here == ')')
           || (*here == ',')
          )
        )
  {
    if (*here == '\0')
      return(--here);
    here++;
  }
  return here;

} // left word

char* left_word( char* left, char* here )
/* find the left end of a word */
{
// exit if alread at left of string field
  if ( left == here )
    return left;
// move left one char, to move out of current word if at its left
  here--;

// if not in a word, find a word
  while ( !(
                isalnum(*here)
             || (*here == ')')
             || (*here == ',')
           )
        )

  {
    if  ( left == here ) return left;
    here--;
  }// while

// find first non word char
  while (
             isalnum(*here)
          || (*here == ')')
          || (*here == ',')
        )
  {
    if ( left == here ) return left;
    here--;
  }// while
// move up to first char of word
  return ++here;
} // left word

void debuf( char** str )
// useage: debuf(&str)
// modifies <str> to remove leading spaces and trailing spaces
{
  char * p1, * p2, * p3;
//  find the first non-space
  p1 = *str;

  if (p1==NULL) return;
  while (*p1 == ' ') p1++;

// find last non-space
  p2 = *str+ strlen(*str)-1;
  while ((*p2 == ' ') || (*p2 == '\n')) p2--;

  if (p2[1] != '\0')
  {                               // with trailing spaces
    char saved_char = p2[1];
    p2[1] = '\0';                 // mark to be end of returned string
    p3 = new_str(p1);             // make a copy to be returned
    p2[1] = saved_char;           // remove mark
    delete(*str);
  } else {
    p3 = new_str(p1);             // make a copy to be returned
    delete(*str);
  }// fi

  *str = p3;                      // return the copy
}// debuf

char* debuf( char* str )
// returns a copy of <str> without leading and trailing spaces
// WARNING str is _NOT_ *delete*d: return value is a copy.
{
  char * p1, * p2, * p3;
//  find the first non-space
  p1 = str;

  while (*p1 == ' ') p1++;

// find last non-space
  p2 = str+ strlen(str)-1;
  while ((*p2 == ' ') || (*p2 == '\n')) p2--;

  if (p2[1] != '\0')
  {                               // with trailing spaces
    char saved_char = p2[1];
    p2[1] = '\0';                 // mark to be end of returned string
    p3 = new_str(p1);             // make a copy to be returned
    p2[1] = saved_char;           // remove mark
  } else {
    p3 = new_str(p1);             // make a copy to be returned
  }// fi

  return p3;                      // return the copy
}// debuf

char* buff_str( char* str, int size, char buf_char )
// usage: newstr = buff_str(str, size, buf_char)
// WARNING str is _NOT_ *delete*d:
// returns a copy of <str> with <size> leading <buf_char>'s
//      and <size> trailing <buf_char>'s

// if you want to modify <str>: buff_str(&str)
{
  char *tmp, *p1, *p2;

// new string to return; use new_str so we can test for '\0'
  tmp = new_str( (2*size) + (strlen(str)) );

// buff left
  p1 = tmp;
  p2 = tmp + size;
  while (p1 != p2)
  {
    *p1 = buf_char;
    p1++;
  }//while
// copy str into tmp
  p2 = str;
  while (*p2 != '\0')
    *p1++ = *p2++;      // copy by char, then increment both positions
// buff right
  while ( *p1 != '\0' )
    *p1++ = buf_char;   // copy buff_char, then move to next position

  return tmp;
}// buff

void buff_str( char** str, int size, char buf_char )
// useage: buff_str(&str)
// <str> is modified
{
  char *tmp, *p1, *p2;

// new string to return; use new_str so we can test for '\0'
  tmp = new_str( (2*size) + (strlen(*str)) );

// buff left
  p1 = tmp;
  p2 = tmp + size;
  while (p1 != p2)
  {
    *p1 = buf_char;
    p1++;
  }//while
// copy str to tmp
  p2 = *str;
  while (*p2 != '\0')
    *p1++ = *p2++;      // copy by char, then increment both positions
// buff right
  while ( *p1 != '\0' )
    *p1++ = buf_char;   // copy buff_char, then move to next position

  delete *str;
  *str = tmp;
}// buff

int is_empty(char*s)
// returns non-zero if string contains only blanks.
{
  int width = strlen(s);
  while (width)
  {
    if (s[(width-1)] != ' ') return 0;
    width--;
  }
  return 1;
}// is empty

void str_upp( char *str )
// usage: str_upp( &str )
// converts all letters to upper case using toupper(char)
{
  char *s;
  if (str != NULL) {                  // crash protection
    for (s = str; (*s != '\0'); s++)  // treat each character
      *s = (char) toupper(*s);        // use toupper on each char
  }//fi
}// str upp



// end of SAT.CPP
