/* character.h - character object for gm_tools project
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

		  
#ifndef CHARACTER_H
#define CHARACTER_H 	 
		  
#include <stdio.h>
		  
#include "stat.h" 
#include "w_rank.h"
#include "skill.h"
#include "disadv.h"
#include "perk.h" 
#include "pow.h"  
#include "gear.h" 
#include "bio.h"

#define FILE_FIELD 20 // size of the input field for the file name
#define NUM_STATS 18 // number of character statistics

// PAGES title, stats, combat, skills, perks, gear, powers, disadvantages
#define PAGES       8
#define TITLE_PAGE  1  	       
#define STAT_PAGE   2  	       
#define COMBAT_PAGE 3  	       
#define SKILL_PAGE  4  	       
#define PERK_PAGE   5  	       
#define GEAR_PAGE   6  	       
#define POWER_PAGE  7  	       
#define DISADVANTAGE_PAGE 8    
		  
class character	  
{ 		  
public:
  character();
  ~character();
    
  int edit(int cmd);	    
  void help(); // provide a list of features and keys to control them 
			    
  void cursor_up(); 	    
  void cursor_down();  	    
    // move the cursor on the current page
  void change_page(int page_move);
    // move the specified number of pages, normaly +1 or -1
    // wrap around occurs if the last or first page is reached
  int change_page_to(int new_page);
    // change to the specified page
    // returns SUCCESS or an errVALUE (see dandefs.h)
  int delete_current();
    // delete the current ability
       		   
    
  int change_current_value(int amt);				
    // change the value of the ability currently selected by amt
    // returns a value from dandefs.h error codes
  int cursor_edit();				 
    // edit the item at the cursor		 
    // returns a value from dandefs.h error codes
  int cursor_make_new();
    // generate a new ability
    // returns a value from dandefs.h error codes
  int edit_name();
  int edit_age();
  int edit_race();
  int edit_sex();
  int edit_living();
  int edit_base();
  int edit_xp();
  int edit_disadv();
  int bio_edit();
   
  void del_stats();
  void del_weapon_ranks();
  void del_skills();
  void del_perks();
  void del_gear();
  void del_powers();
  void del_disadvantages();
  
  void show();
  void show_subtotal_stats();
  void print_subtotal_stats(FILE *stdprn);
  void print_character();
  
  void comment();
  
  int save(FILE *out = NULL);
  int load(FILE *in = NULL);
  
protected:
  int page;
  int stat_top; // cursor y position of displayed strength
  int stat_btm;
  int cury;     // absolute y ordinate for the character line cursor
  char * file_name;
  
  ability * current;
  ability * field_default(int page);
    // returns a default field for the page to be activated when
    // changing page.
  void setup_dummy_nodes();
   // part of the constructor that is used elsewhere eg load()
  
// stats etc. preceeded with ch_ (characteristic)
  
// Bio
  char *ch_name;
  char *ch_living;
  char *ch_race;
  int ch_age;
  char *ch_sex;
  
  int ch_base; // base points
  int ch_max_disadvantages; // maximum points from disadvantages
  int ch_experience;
  bio *ch_bio; 
   
// special data eg stunned, unconcous
  int ch_wounds;
   
// points in stats
  statistic *ch_stats;   // dummy head node
  		   
  statistic *ch_str;
  statistic *ch_dex;
  statistic *ch_con;	       	     
  statistic *ch_body;		     
  statistic *ch_int;	
  statistic *ch_tech; // Frame only 
  statistic *ch_dis;  // discipline == Ego / Will
  statistic *ch_pre;		     	
  statistic *ch_com;  // or Apperance 	       	       	
  		    		     
  statistic *ch_pd;    	       	     
  statistic *ch_ed;    	 	     
  statistic *ch_md;    // Frame only 
  statistic *ch_spd;	 
  statistic *ch_rec;   // Hero only	 
  statistic *ch_end;	       	   
  statistic *ch_stn;	   	   
  statistic *ch_mrc;   // Hero only	   
  statistic *ch_man;		   
  statistic *ch_run;		   
  statistic *ch_swm;		   
  		   		   
		   
  weapon_rank * combat;// ptr to dummy head of doubly linked list;
  weapon_rank * c_tail;// ptr to dummy tail node.
		   
  skill * skill_head;
  skill * skill_tail;
		   
  disadvantage * disadvantage_head;
  disadvantage * disadvantage_tail;
		   
  perk * perk_head;
  perk * perk_tail;

  gear * gear_head;
  gear * gear_tail;

  power * power_head;
  power * power_tail;

  int sum_stats();  // returns total cost of stats
  int sum_combat(); // returns total cost of combat
  int sum_skills(); // returns total cost of skills
  int sum_disadvantages(); // returns total value of disadvantages
  int sum_perks();  // returns total cost of perks
  int sum_gear();  // returns total cost of gear in xp
  int sum_powers();  // returns total cost of powers
    
  void show_title_page();
  void show_stat_page();
  void show_combat_page();
  void show_skill_page();
  void show_perk_page();
  void show_gear_page();
  void show_power_page();
  void show_disadvantage_page();
    
//  void setname(char * new_name);
  ability * get_base(char * data);
    
  void goset(int x, int y);
    
  char * dialog_title(char * field);
};//character (object)


#endif
// end - character.h
