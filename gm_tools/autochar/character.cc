/* character.cc - character object definitions for autochar
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


//#define NDEBUG
#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "common.h"
#include "character.h"
#include "ability.h"
#include "cost.h"
#include "figured.h"
#include "stat.h"
#include "prompt.h"
#include "err_log.h"

// Lines on the title/bio page (bio_edit()) (relative to top+buf)
#define NAME_LINE   1  	       
#define AGE_LINE    2  	       
#define RACE_LINE   3  	       
#define SEX_LINE    4  	       
#define LIVING_LINE 5	       
#define	DISADV_LINE 16         
#define BASE_LINE   17	       
#define EXPERIENCE_LINE	18     
#define END_TITLE_PAGE 18      
 	     	       	       
// replace the box draw variables this can be replaced when windowing
//   functionality is added tk   
#define lft 10		       
#define top 2  	       	       
#define btm 48		       
#define buf 0 // lines of space between border and writing
	     		     
// top most edditable line   
#define cur_y_default top + buf + 1
	     		       
///////////////////////////////////////////////////////////////////////////
////////////////    CONVERSIONS AND LOOKUP     ////////////////////////////
///////////////////////////////////////////////////////////////////////////
	     	       	       
const char TITLE_SKIP[11] = {6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0};
  // Null terminated string containg numbers of blank lines
  // on the title page to be skipped in cursor movement
  // relative to top of character, ie line number is TITLE_SKIP + top + buf
	     	     	       	
ability * character::get_base(char * temp)
// finds and returns a pointer to the base ability (stat) for a skill
{		       	       	
  // insuficent input: general skill
  if (temp == NULL) return NULL;
  if (strlen(temp) < 3) return NULL;
		       	       
  // standard cases    	       
  if (strcmp(temp, "STR") == 0)
    return ch_str;     	       
  else if (strcmp(temp, "DEX") == 0)
    return ch_dex;     	       
  else if (strcmp(temp, "CON") == 0)
    return ch_con;     	       
  else if (strcmp(temp, "BOD") == 0)
    return ch_body;       	       
  else if (strcmp(temp, "INT") == 0)
    return ch_int;     	       
  else if (strcmp(temp, "EGO") == 0) // old name for disipline
    return ch_dis;     	       
  else if (strcmp(temp, "DIS") == 0)
    return ch_dis;     	       
  else if (strcmp(temp, "PRE") == 0)
    return ch_pre;     	       
  else if (strcmp(temp, "COM") == 0)
    return ch_com;     	       
  else		       	       
  // default to general skill  
    return NULL;       	       
} // get base;	       	       
		       	       
		       	       
ability * character::field_default(int page)
// ability defaults for pages (default cursor position)
{		       	       
  switch (page)	       	       
  {		       	       
    case (TITLE_PAGE): 	       
    {		       	       
      return NULL;     	       
    }		       	       
    case (STAT_PAGE):  	       
    {		       	       
      return ch_stats->next;   
    }		       	       
    case (COMBAT_PAGE):	       
    {		       	       
      if (combat->next == NULL)
        return c_tail; 	       
      else	       	       
        return combat->next;   
    }		       	       
    case (SKILL_PAGE): 	       
    {		       	       
      if (skill_head == NULL)  
        return skill_tail;     
      else	       	       
        return skill_head->next;
    }		       	       
    case (PERK_PAGE):  	       
    {		       	       
      if (perk_head == NULL)   
        return perk_tail;      
      else	       	       
        return perk_head->next;
    }		       	       
    case (GEAR_PAGE):  	       
    {		       	       
      if (gear_head == NULL)   
        return gear_tail;      
      else	       	       
        return gear_head->next;
    }		       	       
    case (POWER_PAGE): 	       
    {		       	       
      if (power_head == NULL)  
        return power_tail;     
      else	       	       
        return power_head->next;
    }		       	       
		       	       
    case (DISADVANTAGE_PAGE):  
    {		       	       
      if (disadvantage_head == NULL)
        return disadvantage_tail;
      else	       	       
        return disadvantage_head->next;
    }		       	       
    default:	       	       
    {		     
      say_status("ERROR: page out of range.");
      log_event("ERROR: page out of range.\n");
      return NULL;     	       		    
    }		       	       		    
  }//switch	       	       		    
}//field default       	       		    
		       	       		    
int character::sum_stats()     		    
{		       	       		    
  int count;	       	       		    
  ability * look;      	       		    
		       	       		    
  look = ch_stats->next; // the first statistic;
		       	       		    
  // sum the points spent (in xp)	    
  count = 0;   	       	       	       	    
  while (look != NULL) 			    
  {	   	       			    
    count += look->get_pts();		    
    look = look->next; 			    
  }	   	       			    
	   	       			    
  return count;	       			    
}// character:: sum stats		    
	   	       			    
int character::sum_combat()		    
{	   	       			    
  // check there is at least one combat ability to sum
  assert(combat != NULL);
  if (combat->next == NULL)
    return 0;	       
  else	   	       
    return combat->next->sum();  // call ability::sum - base class method
}// sum combat	       
	   	       
int character::sum_skills()
{	   	       
  // check there is at least one skill to sum
  assert(skill_head != NULL);
  if (skill_head->next == NULL)
    return 0;	       
  else	   	       
    return skill_head->next->sum();  // call ability::sum - base class method
}// sum skills	       
	   	       
int character::sum_perks()
  // returns total cost of perks
{	   	       
  assert(perk_head != NULL);
  if (perk_head->next == NULL)
    return 0;	       
  else	   	       
    return perk_head->next->sum();
}// sum perks	       
	   	       
int character::sum_gear()
  // returns total cost of gear
{	   	       
  assert(gear_head != NULL);
  if (gear_head->next == NULL)
    return 0;	       
  else	   	       
    return gear_head->next->sum();
}// sum gear	       
	   	       
int character::sum_powers()
  // returns total cost of powers
{	   	       
  assert(power_head != NULL);
  if (power_head->next == NULL)
    return 0;	       
  else	   	       
    return power_head->next->sum();
}// sum powers	       
	   	       
     	   	       
int character::sum_disadvantages()
{    	   	       
  assert(disadvantage_head != NULL);
  if (disadvantage_head->next == NULL)
    return 0;	       
  else	   	       
    return disadvantage_head->next->sum();
} // sum disadvantages   
     	   	       
///////////////////////////////////////////////////////////////////////////
//////////    CONSTRUCTOR & INITAILIZATION    /////////////////////////////
///////////////////////////////////////////////////////////////////////////
     	   	       
character::character() 
{    	   	       
// important places    
  page = 1;	       
  stat_top = cur_y_default;
  stat_btm = stat_top + NUM_STATS;  // record the bottom of the stats
  cury = cur_y_default;

// characteristics     
  ch_name = new_str("Joe");
//  title = buff_str(new_str(ch_name));
     		       
  file_name = new_str(FILE_FIELD);
  strncpy(file_name, ch_name, strlen(ch_name));
  strncpy((file_name+strlen(ch_name)), ".chr", 4);
     		       
  ch_age = 25;	       
  ch_living = new_str("Soldier");
  ch_race = new_str("Human");
  ch_sex = new_str("Male");
		       
  ch_base = 750;       
  ch_max_disadvantages = 750;
  ch_experience = 0;   		    

  ch_bio = new bio(); 
  ch_wounds = 0;
  
  ch_stats = new statistic(NULL, NULL, 0, 0); // dummy head node
  // set up the stats depending on HERO rules or Frame rules
#ifdef HERO 	  		    
// HERO stats	  		    
  ch_str = new statistic(ch_stats, "Strength", 0, cSTR);
  ch_dex = new statistic(ch_str, "Dexterity", 0, cDEX);
  ch_con = new statistic(ch_dex, "Constitution", 0, cCON);
  ch_body = new statistic(ch_con, "Body", 0, cBODY);
  ch_int = new statistic(ch_body, "Intelegence", 0, cINT);
  ch_dis = new statistic(ch_int, "Ego", 0, cDIS);
  ch_pre = new statistic(ch_dis, "Presence", 0, cPRE);
  ch_com = new statistic(ch_pre, "Comliness", 0, cCOM);
  ch_pd  = new normal_def((ability*) ch_com, "PD", 0, (ability*) ch_str);
  ch_ed  = new normal_def((ability*) ch_pd,  "ED", 0, (ability*) ch_con);
  ch_spd = new spd((ability*) ch_ed, "Speed",     0, (ability*) ch_dex);
  ch_end = new end((ability*) ch_spd, "Endurance", 0, (ability*) ch_con);
  ch_stn = new stn((ability*) ch_end, "Stun", 0, (ability*) ch_body, (ability*) ch_str, (ability*) ch_con);
  ch_rec = new rec((ability*) ch_stn, "Recovery", 0, (ability*) ch_str, (ability*) ch_con);
  ch_man = new end((ability*) ch_rec, "Mana",     0, (ability*) ch_dis);
  ch_mrc = new rec((ability*) ch_man, "Mana Recovery",0, (ability*) ch_int, (ability*) ch_dis);
  ch_run = new run((ability*) ch_mrc, "Running", 0);
  ch_swm = new swm((ability*) ch_run, "Swimming", 0);
  	     	  		    
#else 	      	  		    
// Frame stats	  		    
  ch_str = new statistic(ch_stats, "Strength", 0, cSTR);
  ch_dex = new statistic(ch_str, "Dexterity", 0, cDEX);
  ch_con = new statistic(ch_dex, "Constitution", 0, cCON);
  ch_body= new statistic(ch_con, "Body", 0, cBODY);
  ch_int = new statistic(ch_body, "Intelegence", 0, cINT);
  ch_tech= new statistic(ch_int, "Tech", 0, cTECH); 
  ch_dis = new statistic(ch_tech,"Will", 0, cDIS);
  ch_pre = new statistic(ch_dis, "Presence", 0, cPRE);
  ch_com = new statistic(ch_pre, "Apperance", 0, cCOM);
  ch_pd  = new normal_def((ability*) ch_com, "PD",        0, (ability*) ch_str);
  ch_ed  = new normal_def((ability*) ch_pd,  "ED",        0, (ability*) ch_con);
  ch_md  = new normal_def((ability*) ch_ed,  "MD", 0, (ability*) ch_dis);
  ch_spd = new spd((ability*) ch_md, "Reflex", 0, (ability*) ch_dex);
  ch_end = new end((ability*) ch_spd, "Endurance", 0, (ability*) ch_con);
  ch_stn = new stn((ability*) ch_end, "Stun", 0, (ability*) ch_body, (ability*) ch_str, (ability*) ch_con);
  ch_man = new end((ability*) ch_stn, "Mana",     0, (ability*) ch_dis);
  ch_run = new run((ability*) ch_man, "Running", 0);
  ch_swm = new swm((ability*) ch_run, "Swimming", 0);
  		  		    				 
#endif  	  		    				 
  current = field_default(page);
  // set nodes to null - first use
  combat = NULL;   	  
  skill_head = NULL; 	  
  perk_head = NULL;	  
  gear_head = NULL;	  
  power_head = NULL;  	  
  disadvantage_head = NULL;
  c_tail = NULL;        	  
  skill_tail = NULL;  	  
  perk_tail = NULL;   	  
  gear_tail = NULL;   	  
  power_tail = NULL;  	  
  disadvantage_tail = NULL;
  
  setup_dummy_nodes();
}// character constructor
  	     	
void character::setup_dummy_nodes()
{ 	     	
  assert(combat == NULL);   	  
  assert(skill_head == NULL); 	  
  assert(perk_head == NULL);	  
  assert(gear_head == NULL);	  
  assert(power_head == NULL);  	  
  assert(disadvantage_head == NULL);
  assert(c_tail == NULL);      	  
  assert(skill_tail == NULL);  	  
  assert(perk_tail == NULL);   	  
  assert(gear_tail == NULL);   	  
  assert(power_tail == NULL);  	  
  assert(disadvantage_tail == NULL);
  	       		       	  
  log_event("character deleted\n");
// restrore dummy nodes	       	  	  	  
  	       		       	  		  
  // weapon ranks dummy head node 	  	  
  combat = new weapon_rank(NULL, NULL, 0);	  
  c_tail = new weapon_rank(combat, NULL, 0);	  
  log_event("weapon rank chain restored\n");	  
  	     		 	  		  
  // skill dummy nodes	       	     		  
  log_event("skill head: %d\n", skill_head); 
  if ((skill_head = new skill(NULL, NULL, 0, NULL)) == NULL)
  {	     		   	  
    log_event("Failed to allocate memory\n");
    fprintf(stderr, "Failed to allocate memory\n");
    exit(EXIT_FAILURE);		  
  } 	     			  
  	     			  
  log_event("skill head restored\n");
  skill_tail = new skill(skill_head, NULL, 0, NULL);
  log_event("skill chain restored\n");
  	       	  	       	  	  
  // perk dummy nodes	       	  	  
  perk_head = new perk(NULL, NULL, 0);  
  perk_tail = new perk(perk_head, NULL, 0);
  log_event("perk chain restored\n");
  	       	  	       	  	  
  // gear dummy nodes	       	  	  
  gear_head = new gear(NULL, NULL, 0);  
  gear_tail = new gear(gear_head, NULL, 0);
  log_event("gear chain restored\n");
  	       	 	       	  	  
  // power dummy nodes	       	  	  
  power_head = new power(NULL, NULL, 0);
  power_tail = new power(power_head, NULL, 0);
  log_event("power chain restored\n");
  	       	   	       	  	  
  // disadvantage dummy nodes  	  	  
  disadvantage_head = new disadvantage(NULL, NULL, 0);
  disadvantage_tail = new disadvantage(disadvantage_head, NULL, 0);
  log_event("disadvantage chain restored\n");
  	       	 	       	  
  assert(combat != NULL);      	  
  assert(skill_head != NULL);  	  
  assert(perk_head != NULL);   	  
  assert(gear_head != NULL);   	  
  assert(power_head != NULL);  	  
  assert(disadvantage_head != NULL);
  assert(c_tail != NULL);      	  
  assert(skill_tail != NULL);  	  
  assert(perk_tail != NULL);   	  
  assert(gear_tail != NULL);   	  
  assert(power_tail != NULL);  	  
  assert(disadvantage_tail != NULL);
			       	  
  log_event("Nodes renewed\n");	  
} // setup dummy nodes

///////////////////////////////////////////////////////////////////////////
///////////     DESTRUCTOR AND DELEATION     //////////////////////////////
///////////////////////////////////////////////////////////////////////////
	       	       
	       	       
void character::del_stats()
{	       	       
  statistic * look, *prev;
	       	       
  look = ch_stats;     
  while (look != NULL) 
  {	       	       
    prev = look;       
    look = (statistic*) look->next;
    delete prev;  
  }	       	  
  ch_stats = NULL;
}//del stats   	       
	       	       
void character::del_weapon_ranks()
{	       	       
  weapon_rank * look, *prev;
	       	       
  look = combat;       
  while (look != NULL) 
  {	       	       
    prev = look;       
    look = (weapon_rank*) look->next;
    delete prev;  
  }	       	  
  combat = NULL;  
  c_tail = NULL;  
}//del weapon ranks    
	       	       
void character::del_skills()
{	       	       
  skill *look, *prev;  

  log_event("begin character::del_skills\n");
  look = skill_head;   	
  while (look != NULL) 			    
  {	       	       			    
    prev = look;       			    
    look = (skill*) look->next;		    
    log_event("character::del_skills - prev (to be deleted): %d, look: %d\n",
	      prev, look);		    
    delete prev;   		    	    
    log_event("character::del_skills - skill deleted\n");
  } 		   			  
  skill_head = NULL; skill_tail = NULL;	  
  log_event("end - character::del_skills\n");
}//del weapon ranks    	       	       
    	       	       		       
void character::del_perks()	       
{	       	       		       
  perk *look, *prev;   		       
	       	       		       
  look = perk_head;    		       
  while (look != NULL) 		       
  {	       	       		       
    prev = look;       		       
    look = (perk*) look->next;	       
    delete prev;  
  }	       	  
  perk_head = NULL;
  perk_tail = NULL;
}//del weapon ranks    		       
  	       	       		       
void character::del_gear()	       
{ 	       	       		       
  gear *look, *prev;   
  	       	       
  look = gear_head;    
  while (look != NULL) 
  {	       	       
    prev = look;       
    look = (gear*) look->next;
    delete prev;  
  } 		  
  gear_head = NULL;
  gear_tail = NULL;
  		  
}//del gear	       
  		       
void character::del_powers()
{ 		       
  power *look, *prev;  
  		       
  look = power_head;   
  while (look != NULL) 
  {		       
    prev = look;       
    look = (power*) look->next;
    delete prev;       
  }		  
  power_head = NULL;
  power_tail = NULL;
}//del weapon ranks    
  		       
void character::del_disadvantages()
{ 		       
  disadvantage *look, *prev;
  		       
  look = disadvantage_head;
  while (look != NULL) 
  {		       
    prev = look;       
    look = (disadvantage*) look->next;
    delete prev;       
  }		  
  disadvantage_head = NULL;
  disadvantage_tail = NULL;
}//del weapon ranks    						   
  	     	       						   
character::~character()						   
{ 	     	       						   
  log_event("character::~character()\n");			   
  if (ch_race != NULL) {					   
    log_event("character::destructor - ch_race: %d %s\n", ch_race, ch_race);
    delete ch_race;
    ch_race = NULL;
  }		   
// delete abilities    		    				   
  del_stats();	       		    				   
  del_weapon_ranks();  log_event("character::destructor - weapons deleted\n");
  del_skills();	       log_event("character::destructor - skills deleted\n");
  del_perks();	       		    				   
  del_gear();	       		    				   
  del_powers();	       		    				   
  del_disadvantages(); 		    				   
  log_event("character::destructor - abilities deleted\n"); 	   
  	   	       		    				   
  if (ch_name != NULL) delete ch_name;				   
  log_event("character::destructor - ch_name deleted\n"); 	   
  if (ch_living != NULL) delete ch_living;  			   
  log_event("character::destructor - ch_living deleted\n"); 	   
  if (ch_race != NULL) {					   
    log_event("character::destructor - ch_race: %d %s\n", ch_race, ch_race);
    delete ch_race;
    ch_race = NULL;
  }		   
  log_event("character::destructor - ch_race deleted\n"); 
  if (ch_sex != NULL) delete ch_sex;	      
  log_event("character::destructor - ch_sex deleted\n"); 
  if (file_name != NULL) delete file_name;    
  log_event("character::destructor - file_name deleted\n"); 
  if (ch_bio != NULL) delete ch_bio;	
  log_event("character::destructor - end\n");
}//char destructor     		   
  	     	       
///////////////////////////////////////////////////////////////////////////
///////////    DISPLAY        /////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
  	     	       
void character::show_subtotal_stats()
{ 
  log_event("character::show_subtotal_stats\n");
// redisplay the subtotal for stats
  int sub_total = sum_stats();
  text_norm(); 	
  dcur_move(lft+buf, stat_btm+buf);
  dwrite("          Total Statistics Costs: %6d     ", sub_total);
  log_event("character::show_subtotal_stats - end\n");
}	       	       
	       	       
void character::print_subtotal_stats(FILE * stdprn)
{	       	       
// redisplay the subtotal for stats
  int sub_total = sum_stats();
  fprintf(stdprn, "          Total Statistics Costs: %6d     \r\n", sub_total);
}//print subtotal stats
       	       	       
void character::show_stat_page()
{ 
  log_event("character::show_stat_page\n");
  text_highlight();   	       			   
  // heading for stats columns
  dcur_move(lft+buf, top+buf);
  dwrite("Val Characteristic  Base  Cost  Max  Pts");
       	       	       		   
  text_norm();	      
  // show the stats   		   
  ability *look;     		   
  look = ch_stats->next; // first ability in list;
  int i = top+buf+1; 	      		   
  while (look != NULL)	   
  { 
    log_event("character:show_stat_page - about to crash\n");
    look->show(lft+buf, i);
    log_event("character:show_stat_page - after crash\n");
    look = look->next; i++;	   
  }  	       	   		   
  show_subtotal_stats();	   
  log_event("character:show_stat_page - after show subtotal\n");
       	       	   		   
  // show highlighted stat	   
  text_highlight();
  // assert(cury > stat_top-1);	   
  log_event("character:show_stat_page - before show highlight\n");
  current->show(lft+buf, cury);
  log_event("character::show_stat_page - end\n");
}//show_stat_page		   
  	       			   
void character::show_combat_page() 
{ 	       
  ability *look;     	     
  int total = 0; //total combat cost
  int i;
  	
  text_highlight();
  dcur_move(lft+buf, top+buf);
  dwrite("Ranks  Field             @cost  pts  Notes");
  	       	
  text_norm();	
  look = combat->next;
  i = top+buf+1;
  while (look != NULL)
  {	       
    look->show(lft+buf, i);
    total += look->get_pts();
    look = look->next;
    i++;       
  } // while
  text_highlight();
  current->show(lft+buf, cury);
      	       
  total -= c_tail->get_pts(); // not a real node
   	     
  dcur_move(lft+buf, i+1);
  text_norm();
  dwrite("Total Combat Cost: %d", total);
  drefresh();
}//show_combat_page    		
      	       	       		
      	       	       		
void character::show_skill_page()
{     	       	       		
  int total = 0;	       		
      	       	       
  text_highlight();
  dcur_move(lft+buf, top+buf);
  dwrite("Val  Skill                Base Stat    @cost  pts  Notes");
   	       	       		
  text_norm();
  ability * look = skill_head->next;
  int i = cur_y_default;
  while (look != NULL) 		
  {	       			
    if (look != current)	
    {	       			
      look->show(lft+buf, i);	
    }	       			
    else       			
    {	       			
      text_highlight();	
      look->show(lft+buf, i);	
      text_norm();	
    }  	       		
    total += look->get_pts();	
    look = look->next;	
    i++;		
  }			
  dcur_move(lft+buf, i+1);			
  dwrite("Total Skill Cost: %d", total);
}// show skill page	
			
void character::goset(int x, int y)
{		    
  dcur_move(x, y);
  if (y == cury)    	
    text_highlight();	
  else	      		
    text_norm();	
} 	      		
  	      		
void character::show_title_page()
{ 	      		
  int x, y; // my cursor
  int stat_total, combat_total, skill_total;
  int gear_total, perk_total, power_total;
  int disadvantage_total, disadvantage_contribution;
  int total_spent, balance;
  	      
  x = lft+buf+1;  
  y = top+buf+1;  
   	      
  total_spent = 0;
  total_spent += stat_total = sum_stats();
  total_spent += combat_total = sum_combat();
  total_spent += skill_total = sum_skills();
  total_spent += perk_total = sum_perks();
  total_spent += gear_total = sum_gear();
  total_spent += power_total = sum_powers();
       	  
  disadvantage_total = sum_disadvantages();
  disadvantage_contribution = (disadvantage_total > ch_max_disadvantages) ? 
                              ch_max_disadvantages : disadvantage_total;
  balance = total_spent - ch_base - disadvantage_contribution - ch_experience;
       	     								     
  goset(x, y); dwrite("Name: %s", ch_name); y++;
  goset(x, y);		   
  if (ch_age < 0)      	   
    dwrite("Age: Unknown / Ageless");
  else	    
    dwrite("Age: %d", ch_age);
  y++;	  	      
  goset(x, y); dwrite("Race: %s", ch_race); y++;
  goset(x, y); dwrite("Sex: %s", ch_sex); y++;
  goset(x, y); dwrite("Living: %s", ch_living); y++;
	  
  y++;
  goset(x, y); dwrite("Stat Cost:    %5d", (stat_total));   y++;
  goset(x, y); dwrite("Combat Cost:  %5d", (combat_total)); y++;
  goset(x, y); dwrite("Skill Cost:   %5d", (skill_total));  y++;
  goset(x, y); dwrite("Perk Cost:    %5d", (perk_total));   y++;
  goset(x, y); dwrite("Gear Cost:    %5d", (gear_total));   y++;
  goset(x, y); dwrite("Power Cost:   %5d", (power_total));  y++;
  goset(x, y); dwrite("-------------------" ); y++;
  goset(x, y); dwrite("Total:        %5d", (total_spent));        y++;
  y++;
  if (disadvantage_total > ch_max_disadvantages)
  {
    goset(x, y); 
    dwrite("Disadvatages: %5d (Total: %d)", disadvantage_contribution, disadvantage_total); 
    y++;
  }
  else
  {
    goset(x, y); 
    dwrite("Disadvatages: %5d (Max: %d)", disadvantage_total, ch_max_disadvantages); 
    y++;
  }
  goset(x, y); dwrite("Base:         %5d", (ch_base));      y++;
  goset(x, y); dwrite("Experience:   %5d", (ch_experience)); y++;
  goset(x, y); dwrite("-------------------" ); y++;
  goset(x, y);			
  if (balance > 0)		
    dwrite("Overspent:    %5d", balance);
  else			
    dwrite("Unspent:      %5d", (-balance));
}// show title page	
     			
void character::show_perk_page()
{    			
  int row;		
  int total = 0;		
		
  row = cur_y_default;
  text_highlight();
  dcur_move(lft+buf, top+buf);  	   		
  dwrite("Perk                  Pts");
  		     	
  text_norm();
  ability * look = perk_head->next;
  while (look != NULL)	
  {		     
    if (look != current)
    {	    	     
      look->show(lft+buf, row);
      total += look->get_pts();
    }		     
    else	     
    {		     
      text_highlight();
      look->show(lft+buf, row);
      text_norm( );  
      total += look->get_pts();
    } 		     
    look = look->next;
    row++;	     
  }   		     
		
  dcur_move(lft+buf, row+1);
  dwrite("Total Perks: %d", total);
}// show perk page   	 	  
	 	     	 	  
void character::show_gear_page()  
{	 	     	 	  
  int total= 0;	     	 	  
	   	
  text_highlight();
  dcur_move(lft+buf, top+buf);
  dwrite("Gear                  Pts");
	    	     	 	  
  text_norm();
  ability * look = gear_head->next;
  int i = cur_y_default;
  while (look != NULL)		  
  {	  			  
    if (look != current)	  
    {	  			  
      look->show(lft+buf, i);	  
      total += look->get_pts();	  
    }	  			  
    else  			  
    {				  
      text_highlight(); 
      look->show(lft+buf, i);	  
      text_norm(  );	  
      total += look->get_pts();	  
    }	       			  
    look = look->next;		  
    i++;       			  
  }	       

  dcur_move(lft+buf, i+1);   	
  dwrite("Total Gear: %d", total);
}// show gear page     		
	       	       		
void character::show_power_page()
{	       	       		
  int total = 0;	       		
	 	       
  text_highlight();
  dcur_move(lft+buf, top+buf);
  dwrite("Power                  Pts");
       	   	       		
  text_norm();
  ability * look = power_head->next;
  int i = cur_y_default;
  while (look != NULL) 	
  {    	   		
    if (look != current)
    {  	   		
      look->show(lft+buf, i);
      total += look->get_pts();
    }  	   		
    else   		
    {  	   		
      text_highlight( );   
      look->show(lft+buf, i);
      text_norm();	
      total += look->get_pts();
    }		 	
    look = look->next;	
    i++;	 	
  }		 	

  dcur_move(lft+buf, i+1); 
  dwrite("Total powers: %d", total);
}// show power page		
	   	 		
	   	 		
void character::show_disadvantage_page()
{	   			
  int disadvantage_total = 0;
	   
  text_highlight();
  dcur_move(lft+buf, top+buf); 
  dwrite("Value  Disadvantage");
		
  text_norm();
  ability * look = disadvantage_head->next;
  int i = cur_y_default;
  while (look != NULL)	 	       
  {	      	    	 	       
    if (look != current) // other lines  	  
    {	      	    	 	       	   
      look->show(lft+buf, i);	       	   
      disadvantage_total += look->get_pts();	  
    }	      	    	 	       
    else      	       	// currnet line       	       
    {	      	    	 	  
      text_highlight();	  
      look->show(lft+buf, i);	  
      text_norm( );  	 
      disadvantage_total += look->get_pts();
    }		    	 
    look = look->next;	 
    i++;	    	 
  }		    	 

  dcur_move(lft+buf, i+1);
  if (disadvantage_total <= ch_max_disadvantages)
  {		    
    dwrite(
	      "Total points from disadvantages: %d (Max: %d)", 
	      disadvantage_total, ch_max_disadvantages);
  }	      		 						     
  else	      		 						     
  {	   
    dwrite(
	      "Total points from disadvantage: %d (Total disadvantages: %d)", 
	      ch_max_disadvantages, disadvantage_total);      	   
  }	     								     
}// show disadvantage page					  	     
  	     							  
void character::show()						  
     	     							  
{    
  dclear();
  clear_status_line();
  switch (page)			
  {	     			
    case(TITLE_PAGE):		
    {	     			
      show_title_page();	
      break;			
    }	  			
    case(STAT_PAGE):		
    // stats page		
    {	  			
      show_stat_page();		
      break;			
    }// stat page		
	  			
    case( COMBAT_PAGE ):	
    // weapon ranks		
    {	  			
      show_combat_page();	
      break;			
    }	  			
	  			
    case ( SKILL_PAGE ):	
    {	  			
      show_skill_page();	
      break;			
	  			
    }//skill page		
	  			
    case ( PERK_PAGE ):		
    {	  			
      show_perk_page();		
      break;			
    }	  			
	  			
    case ( GEAR_PAGE ):		
    {	  			
      show_gear_page();		
      break;			
    }	  			
	  			
    case ( POWER_PAGE ):	
    {	  			
      show_power_page();	
      break;			
    }	  			
	  			
    case ( DISADVANTAGE_PAGE ):	
    {	  			
      show_disadvantage_page();	
      break;			
    }	  			
	  			
    default:			
    {	
      dcur_move(1, 1);
      dwrite("%d", page);
      break;	       		
    }	  	      		
  }	  	      		
} // show 	      		
	  	      		



///////////////////////////////////////////////////////////////////////////
/////////////    USER HELP    /////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
  		      
void character::help()		     
// provide a user help scrren
{ 	  	      
  log_event("user help\n");
  // clear the screen 
  log_event("help - text_norm()\n");
  dclear();     	       	       	    
  say_status("Help - press any key to return to the character");
  text_norm();
  // display the help information  
  dcur_move(1,1);	      	     
  dwrite("ESC - Exit the program.\n");
  dwrite("PGUP - Previous page.\n");
  dwrite("PGDN - Next page.\n");   
  dwrite("INS - Insert entry into list.\n");
  dwrite("DEL - Delete entry from list.\n");
  dwrite("UP/DN arrow - Move through list.\n");
  dwrite("HOME - Move to biography page.\n");
  dwrite("ENTER - Edit information at cursor.\n");
  dwrite("F1 - This help message.\n");  
  dwrite("F2 - Save the character.\n");
  dwrite("F3 - Load a character.\n");	       
  dwrite("CTRL-P - Print the character.\n");
  dwrite("'+' - Increase value.\n");
  dwrite("'-' - Decrease value.\n");
  drefresh();	 
  // wait for any key input
  dgetch_wait(-1);
  dgetch();	  
  dgetch_wait(0);
  // redraw the character  
  clear_status_line();
  show();	
}// help	
   	 	
///////////////////////////////////////////////////////////////////////////
/////////       FILE MANIPULATIONS         ////////////////////////////////
///////////////////////////////////////////////////////////////////////////
		      
		      
int character::save(FILE *out) 
// saves the character
// This method will either save the character within a file passed to it
// or open a suitable file. The adv_game save_game rutine calls this, via
// adv_character::adv_save() with the save game file as a parameter. This 
// rutine is also used to export a character from adv_game, or to save a 
// character from autochar. When the parameter out is NULL the FILE* out is 
// kept 'local' in that it is opened and closed within this rutine. Otherwise
// the file is assumed to be open and is left open.
{	       	      	     
  char * temp; 	      	     
  ability * look;     	     
  prompt *p;   	 	     
  int keep_file_local = FALSE; // set TRUE if the output file is opened here
  		
  log_event("character::save()\n");
  // check to see if a file has been passed for saving to, if not
  // open a file for saving the character
  if (out == NULL)	      
  { 			      
    keep_file_local = TRUE; // lets us know we have to close the file 
    if (file_name != NULL) delete file_name;
    file_name = NULL;	      
    			      
    say_prompt("Enter the file to save to: ");
    p = new prompt();	      
    file_name = new_str(p->s); 		  
    delete p;    	      	     		  
    			      
    debuf(&file_name);  // remove leading and trailing spaces
      	       	 	      
    log_event(" Saving %s to %s        ", ch_name, file_name);
      	       	      	      		  
    if ((out = fopen(file_name, "wt")) == NULL)
    { 	       	      	      		  
       say_status("Cannot open output file.");
       fprintf(stderr, "Cannot open output file.\n");
       return 1; 	      	   		  
    }//fi     	       	      	   		  
  }//fi: create output file   
  log_event("character::save - Output file setup.\n");
  	      		      	
  // save character bio	      		  
  fprintf(out, "%s\n", ch_name);	  
  fprintf(out, "%d\n", ch_age);		  
  fprintf(out, "%s\n", ch_race);	  
  fprintf(out, "%s\n", ch_sex);		  
  fprintf(out, "%s\n", ch_living);	  
  fprintf(out, "%d\n", ch_base);  	  
  fprintf(out, "%d\n", ch_max_disadvantages);
  fprintf(out, "%d\n", ch_wounds);
  fprintf(out, "%d\n", ch_experience); 	
  log_event("character::save - bio saved\n");
  	      			  	
  for (int i = 2; i <= PAGES; i++) // pages 2 to PAGES inclusive
  {	       			  
    log_event("character::save - page: %d\n", i);
  	      				       
    fprintf(out, "*\n");  // tk comment so we know what goes in each 
                          // section by reading the save file
    look = field_default(i);	  	  
    while (look != NULL)	  	  
    {	       			  	  
      look->save(out); // save the stats  
      look = look->next;	  	  
    }//while   			  	  
  }//rof       			  	
  fprintf(out, "* End of character.\n");  // mark end of character
  log_event("character::save - abilities saved\n");
    	       			  	  
  if (keep_file_local)
  {
    fclose(out); 			  	
    clear_prompt();		  	 
    if (ch_name == NULL)		  	
      say_status("Character saved to %s.", file_name);
    else				  	
      say_status("%s saved to %s.", ch_name, file_name);
  }//fi: cleanup for local save
  
  log_event("character::save - end\n");
  return SUCCESS; 	  	  	
}//save	       			  	  
      	       			       	  
int character::load(FILE *in)
{     	       		  	       	  
  char * temp; 		  	       	  
  ability *look, *prev;	  	       	  
  int new_num, i;	  	       	  
  skill * new_skill;	  	       	  
  prompt *p;   
  int keep_file_local = FALSE; // see notes in character::save
      	       		  	       	      
  // open the character file, if not allready specified
  if (in == NULL)			      
  {	       	  			      
    keep_file_local = TRUE;
    say_prompt("Enter the file to load.               ");
    p = new prompt();	  	  	      
    if (file_name != NULL) delete file_name;  
    file_name = new_str(p->s);	       	      
    delete p;    	 	  	      
      	       	 	  	  	      
    if (is_empty(file_name)) return 1;// test for no input == user cancel
      	       	 	  	       	  
    debuf(&file_name);  // remove leading and trailing spaces
      	       	 	  	       	  
    say_status("Loading file.");
    log_event("Loading %s\n", file_name);
      	       		  
    if ((in = fopen(file_name, "rt"))
        == NULL) 		  
    { 	       		  
       log_event("Cannot open input file "); log_event(file_name); log_event("\n");
       say_status("Cannot open input file.");
       return 1; 	    	   	       	  
    }//fi	       		
  }//fi: open input file
  
  // delete bio		   
  log_event("character::load - Deleting bio\n");
  if (ch_name != NULL) delete ch_name; 		
  log_event("character::load - ch_name deleted\n");
  if (ch_living != NULL) delete ch_living;	
  log_event("character::load - ch_living deleted\n");
  if (ch_race != NULL) delete ch_race; 		
  log_event("character::load - ch_race deleted\n");
  if (ch_sex != NULL) delete ch_sex;   		
  log_event("character::load - ch_sex deleted\n");
  if (file_name != NULL) delete file_name;	
  log_event("character::load - file_name deleted\n");
  ch_name = NULL;	   	  		
  ch_living = NULL;	   	  		
  ch_race = NULL;	   	  		
  ch_sex = NULL;	   	  		
  file_name = NULL;	   
  log_event("Bio deleted.\n");
	     		   
  log_event("Deleting abilities.\n");
  for (i = 3; i <= PAGES; i++) 	   // all linked list abilities    	  
  {	       	       	     	  	  
    look = field_default(i); 	  	  
  	       	       	     	  	  
    while (look->next != NULL)
    {  	      		     
      prev = look;	   
      look = look->next;   
      delete prev;	   
    }//while   	       	     	    	  
    log_event("  end of list\n");
  }//rof       	       	     	    	  
  log_event("Abilities deleted.\n");	  
  	       		   
// load character bio information      	    
  temp = new_str(MAX_LINE);	       	  
  // temp is used to load each line of input in
  // the input is then processed by sscanf() or copied with debuf()
  	       			  
  // get ch_name		       	  
  fgets(temp, MAX_LINE, in);	       	     
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  ch_name = debuf(temp);   // copy the read name and trim it
  log_event("Name loaded: "); log_event(ch_name); log_event("\n");
  	       		  	  	     
  // get age   		  	       	     
  fgets(temp, MAX_LINE, in);	       	     
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  sscanf(temp, "%d", &ch_age);	    	     
  log_event("age loaded\n");	  
  	       			    	     
// get race    			    	     
  fgets(temp, MAX_LINE, in);	    	  
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  ch_race = debuf(temp);	   
  	       			    	  
// get sex     			    	  
  fgets(temp, MAX_LINE, in);	    	  
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  ch_sex = debuf(temp);		  
  	       			    	      
// get living  			    	      
  fgets(temp, MAX_LINE, in);  fprintf(stderr, "read ");
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character 
  ch_living = debuf(temp);   	  
  	       			  	      	    
// base points 			  	      	    
  fgets(temp, MAX_LINE, in);	  	      	    
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  sscanf(temp, "%d", &ch_base);	  	  	    
  	       			  	  	       
// max disadvantages		  	  	       
  fgets(temp, MAX_LINE, in);	  	  	       
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  sscanf(temp, "%d", &ch_max_disadvantages);	       
  log_event("character::load - max disadv: %d\n", ch_max_disadvantages);
  
// wounds
  fgets(temp, MAX_LINE, in);
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  sscanf(temp, "%d", &ch_wounds);	       
  log_event("character::load - wounds: %d\n", ch_wounds);
  
// experience
  fgets(temp, MAX_LINE, in);
  temp[(strlen(temp)-1)] = '\0'; // remove new-line character
  sscanf(temp, "%d", &ch_experience);	       
     	       	   	       
  
// stats       			  
  fgets(temp, MAX_LINE, in); // get separating star			  
  assert (ch_stats != NULL);   	      	  
  look = ch_stats->next;   	  
  while (look != NULL) 	     	  	  
  {	       	       	     	  	  
    fgets(temp, MAX_LINE, in);	  
    temp[(strlen(temp)-1)] = '\0'; // remove new-line character
    sscanf(temp, "%d", &new_num); 	
    look->change_pts(new_num-(look->get_pts())); 
    look = look->next; 	       	       	  
  }// while    	       		       	  
  log_event("Stats loaded.\n");   	  
  	       	       		       	  
  fgets(temp, MAX_LINE, in); // get separating star
   	       	       		       	  
  // load abilities    		       	  
  // read in each line, stripping terminating nulls.
  // Send each line to the appropriate ability loading rutine.
  // Move on to the next ability type when an asterix is found on the begining of the line
  for (i = 3; i <= PAGES; i++)	       	  
  {	       	       	     	  	  
    look = field_default(i); 	  	  
  	       	       	     	  	  
    while ((fgets(temp, MAX_LINE, in) != NULL) && (temp[0] != '*'))
    // '*' marks between ability groups	  
    {  
      log_event("remove \\r\n");  
      temp[(strlen(temp)-1)] = '\0'; // remove new-line character
      log_event("process line: %s\n", temp);
      look->load(debuf(temp)); 	       	  
      log_event("end of loop\n ");
    }//while   	       	     	    	  
    log_event("star or end of file\n");
  }//rof       	       	     	    	  
  log_event("\nLoad abilities done.\n");	  
      	       	       	     	   	  
  // set skill bases     	     	   	  
  // do to all in the body of the list but not dummy end nodes!
  new_skill = (skill*) skill_head->next;    		  
  assert(new_skill != NULL); 	   	    
  while (new_skill->next != NULL)    	    
  {	       			     	    
    new_skill->set_base(get_base(new_skill->find_base));
    new_skill = (skill*) new_skill->next; 
  }//while     			   
  
  // clean up    			    
  if (temp != NULL) delete temp;
  cury = cur_y_default;
  current = field_default(page);	      
  if (keep_file_local)
  {
    fclose(in);  			    
    if (ch_name == NULL)			      
      say_status("Character loaded from %s.", file_name);
    else					      
      say_status("%s loaded from %s.", ch_name, file_name);
  }//fi: keep file local
  
  return SUCCESS;    			    
}//load	       			    
    	       			    
  	       
void character::print_character()
{    	       
  ability * look;
  int stat_total, combat_total, skill_total;	   
  int gear_total, perk_total, power_total;	   
  int disadvantage_total, disadvantage_contribution;
  int total_spent, balance;			   
     						   
#ifndef stdprn // if the stdprn is not available - kludge, don't know why this is happening
#define PRINT_TO_FILE  
  FILE * stdprn;				   
  if ((stdprn = fopen("print.txt", "wt"))	   
      == NULL)					   
  {  						   
     fprintf(stderr, "Cannot open output file.\n");
     return;					   
  }//fi	    
  log_event("printing to file\n");
#endif	    		       			   
  fprintf(stdprn, "\r"); // return carage to left edge

  say_status("Printing character");
// Bio Page		       
  log_event("printing bio page\n");			   
     			       	   		   
  total_spent = 0;	       	   			   
  total_spent += stat_total = sum_stats();		   
  total_spent += combat_total = sum_combat();		   
  total_spent += skill_total = sum_skills();		   
  total_spent += perk_total = sum_perks();		   
  total_spent += gear_total = sum_gear();		   
  total_spent += power_total = sum_powers();
     				   
  disadvantage_total = sum_disadvantages();
  disadvantage_contribution = (disadvantage_total > ch_max_disadvantages) ? 
                              ch_max_disadvantages : disadvantage_total;
  balance = total_spent - ch_base - disadvantage_contribution - ch_experience;
     				   
  fprintf(stdprn, "Name: %s\n\r", ch_name);
  if (ch_age < 0)		   
    fprintf(stdprn, "Age: Unknown / Ageless\n\r");
  else				   
    fprintf(stdprn, "Age: %d\n\r", ch_age);
     				   
  fprintf(stdprn, "Race: %s\r\n", ch_race);
  fprintf(stdprn, "Sex: %s\r\n", ch_sex);	    
  fprintf(stdprn, "Living: %s\r\n", ch_living);	    
     				   		    
  fprintf(stdprn, "Stat Cost:    %5d\r\n", (stat_total));
  fprintf(stdprn, "Combat Cost:  %5d\r\n", (combat_total));
  fprintf(stdprn, "Skill Cost:   %5d\r\n", (skill_total));
  fprintf(stdprn, "Perk Cost:    %5d\r\n", (perk_total));
  fprintf(stdprn, "Gear Cost:    %5d\r\n", (gear_total));
  fprintf(stdprn, "Power Cost:   %5d\r\n", (power_total));
  fprintf(stdprn, "-------------------\r\n" );	    
  fprintf(stdprn, "Total:        %5d\r\n", (total_spent));
     						    
  if (disadvantage_total > ch_max_disadvantages)    
    fprintf(stdprn, 
            "Disadvatages: %5d (Total: %d)\r\n", 
     	    disadvantage_contribution, disadvantage_total);
  else
    fprintf(stdprn, 
            "Disadvatages: %5d (Max: %d)\r\n", 
     	    disadvantage_total, ch_max_disadvantages); 
  fprintf(stdprn, "Base:         %5d\r\n", (ch_base));
  fprintf(stdprn, "-------------------\r\n" ); 	
     
  if (balance > 0)
    fprintf(stdprn, "Overspent:    %5d\r\n", (balance));
  else
    fprintf(stdprn, "Unspent:      %5d\r\n", (-balance));
     
// Stat Page
  log_event("printing stat page\n");
  // heading for stats columns
    fprintf(stdprn, "\nVal Characteristic  Base  Cost  Max  Pts");
     
  // show the stats
     
    look = ch_stats->next; // first ability in list;
    while (look != NULL)
    {
      look->print(stdprn);
      look = look->next;
    }
     
    fprintf(stdprn, "\r\n");
    print_subtotal_stats(stdprn);  // print the total
    fprintf(stdprn, "\r\n");
     
// Weapons Page
  log_event("printing weapons page\n");
  fprintf(stdprn, "\r\nRanks  Field             @cost  pts  Notes");
     				       
  look = combat->next;		       
  while (look != NULL)		       
  {  				       
    look->print(stdprn);	       
    look = look->next;		       
  }//while			       
     				       
  fprintf(stdprn, "\r\n\r\nTotal Combat Cost: %d\r\n", combat_total);
     				       
// Skill Page			       
  log_event("printing skill page\n");
  fprintf(stdprn, "\r\nVal  Skill             @cost  pts  Notes");
     				       
  look = skill_head->next;
  while (look != NULL)
  {  
    look->print(stdprn);
    look = look->next;
  }//while
     
  fprintf(stdprn, "\r\n\r\nTotal Skill Cost: %d\r\n", (combat_total));
     
// Perks
  log_event("printing perk page\n");
  fprintf(stdprn, "\r\nPerk                  Pts");
     
  look = perk_head->next;
  while (look != NULL)
  {  
    if (look != current)
    {
      look->print(stdprn);
    }
    look = look->next;
  }  
     
  fprintf(stdprn, "\r\n\r\nTotal Perks: %d\r\n", (perk_total));
     
// Gear
  log_event("printing gear page\n");
  fprintf(stdprn, "\r\nGear                  Pts");
     
  look = gear_head->next;
  while (look != NULL)
  {  
    if (look != current)
    {
      look->print(stdprn);
    }
    look = look->next;
  }  
     
  fprintf(stdprn, "\r\n\r\nTotal Gear: %d\r\n", (gear_total));
     
// powers
  log_event("printing powers page\n");
  fprintf(stdprn, "\r\nPower                  Pts");
     	     
  look = power_head->next;
  while (look != NULL)
  {  	     
    if (look != current)
    {	     
      look->print(stdprn);
    }	     
    look = look->next;
  }  	     
     	     
  fprintf(stdprn, "\r\n\r\nTotal powers: %d\r\n", (power_total));
     	     
     	     
// Disadvantages
  log_event("printing disadvantages page\n");
  fprintf(stdprn, "\r\nVal  Disadvantage");
     	     
  look = disadvantage_head->next;
  while (look != NULL)
  {  
    if (look != current)
    {
      look->print(stdprn);
    }
    look = look->next;
  }  
  if (disadvantage_total > ch_max_disadvantages)
    fprintf(stdprn, 
    "\r\n\r\nTotal points contributed (Max): %5d (Total disadvantages: %d)", 
    (disadvantage_contribution), (disadvantage_total));
  else
    fprintf(stdprn, 				   
    "\r\n\r\nTotal Points From Disadvantage: %d (Max: %d)\r\n",
    disadvantage_total, disadvantage_contribution);
  log_event("printing complete\n");
#ifdef PRINT_TO_FILE  
  fclose(stdprn);    
#endif
  say_status("Print complete");
}//print character
     
     
// **************************************************************************
// ********************         EDITING        ******************************
// **************************************************************************
   									     
   				
void character::cursor_up()	
// move the cursor up on the current page
{  	      	      		
  if (page == TITLE_PAGE)	
  {	     	      		
    if (cury > cur_y_default)	
    { 		      		
      cury--;         		
      // check for blank lines to skip in cursor movement on the
      // title page. <TITLE_SKIP> is a const initalised at the top of
      // this file, so it can be altered any time.
      // strchr() is usually used to search for char in string but we use
      // it to find numbers in an array here.
      while (strchr(TITLE_SKIP, (cury - top - buf)) != NULL)
        cury--;	      		
    }   	      		
    else // alread at top of page so wrap to bottom
      cury = END_TITLE_PAGE + top + buf;		       
          	      				       	       
//    show(); // Would be optimal to redraw just the line we were on 
       	    // and the one we go to but this is much easier 
  }// title page      		
  else if (current == NULL)	
    log_event("*** character::cursor_up - uninitilised current ability\n");
  else if (current->prev == NULL)				       
    log_event("*** character::cursor_up - current points to ability list head\n");
  else if (current->prev->prev != NULL)			   
  // if not at the top line		       		   
  {	     	      			       
    // clear old highlight		       
//    text_norm();    			       
//    current->show(lft + buf, cury);	       
    // move cursor 			       
    cury--;
    current = current->prev;	       
    // draw new highlight  		       
//    text_highlight();      		       
//    current->show(lft + buf, cury);	       
  } // fi: move up in ability list	     	    			       
  	  			
} // cursor up			
  
  
void character::cursor_down()
// move the cursor down on the current page
{   	      	
  if (page == TITLE_PAGE)		       
  { 	     	 			       
    if (cury == (END_TITLE_PAGE+top+buf))	       
    // end of page: wrap		       
      cury = cur_y_default;   	       
    else	    			       
    {	    			       
      cury++;   	 		       
      // check for blank lines to skip in cursor movement on the
      // title page. <TITLE_SKIP> is a const initalised at the top of
      // this file, so it can be altered any time.
      // strchr() is usually used to search for char in string but we use
      // it to find numbers in an array here.
      while (strchr(TITLE_SKIP, (cury-top-buf)) != NULL)
        cury++; 
    }	       
  } // title page   
  else if (current == NULL) 
    log_event("*** character::cursor_down - uninitilised current ability.\n ");
  else if (current->next != NULL)
  {	     	       
    cury++;
    current = current->next;
  } // fi: move down in ability list	     	       
} // cursor down
  
int character::change_current_value(int amt)
// change the value of the ability currently selected by amt
{  	     		     
  if (current != NULL)
  {	     
    current->change_value(amt);
    return 0;
  } else     
    return 1;
  
} // change current value

int character::cursor_edit()  
// edit the item at the cursor
{ 		 
  int err = 0;	 
  // abort if un-editable   
  if (page == TITLE_PAGE) 
    err = bio_edit();	      
  else if (current == NULL)
  {		 
    log_event("*** character::cursor_edit - current is not initialised\n");
    err = errUNINITIALISED;
  }		 	
  else if (current->prev == NULL || current->next == NULL)
    err = cursor_make_new(); 	
  else	       	      
    err = current->edit();
  return err;	 
} // cursor edit      
    	       	 
int character::cursor_make_new()
{   		 
  int err = SUCCESS;
  char * new_base;  // to hold the input used to select a skill base 
  prompt *p; 			      
  
  if (current == NULL) 
  // stat or bio page, can't add to this
    log_event("character::cursor_make_new - can't add ability here\n"); 
  else if (page == SKILL_PAGE)	  	      
  // skills - require entry of a base,    
  //          must be treated differently to other abilities
  {	       	  			      	     
    text_norm();				     
    say_prompt("Enter Stat Base: ");		     
    p = new prompt();   			     
    new_base = debuf(p->s);  // (debuf allocates memory) 	
    delete p;      	      		      	     
    if (is_empty(new_base)) 
    // test for no input == user cancel
    {
      if (new_base != NULL) delete new_base; // if new_base consists of whitespace
      return errUSER_CANCEL;
    }//fi
       	       	       	     	       	      	     
    // make temp into a three letter stat code 	     
    str_upp(new_base); 	       	       		     
    if (strlen(new_base) > 3)      	       	     
      new_base[3] = '\0';          	       	     
       	       	       	      	       	       	     
    // call with apropriat stat      	       	     
    if (strcmp(new_base, "STR") == 0)       // strcmp returns 0 
      err = current->make_new(ch_str);	    // params are equal
    else if (strcmp(new_base, "DEX") == 0) 	     
      err = current->make_new(ch_dex) ;		     
    else if (strcmp(new_base, "CON") == 0) 	   
      err = current->make_new(ch_con) ;
    else if (strcmp(new_base, "BOD") == 0) 	   
      err = current->make_new(ch_body) ;
    else if (strcmp(new_base, "INT") == 0) 	      
      err = current->make_new(ch_int) ;
#ifndef HERO		     
    else if (strcmp(new_base, "TEC") == 0) 	   
      err = current->make_new(ch_tech) ;
#endif	    		     
    else if (strcmp(new_base, "EGO") == 0) // old name for disipline
      err = current->make_new(ch_dis) ;
    else if (strcmp(new_base, "DIS") == 0) 
      err = current->make_new(ch_dis) ;
    else if (strcmp(new_base, "WIL") == 0) 
      err = current->make_new(ch_dis) ;
    else if (strcmp(new_base, "PRE") == 0) 
      err = current->make_new(ch_pre) ;
    else if (strcmp(new_base, "COM") == 0)
      err = current->make_new(ch_com) ;
    else if (strcmp(new_base, "APP") == 0)
      err = current->make_new(ch_com) ;
    else    	       	      	       	 
      // default to general skill    	 
      err = current->make_new(NULL) ;
       	       	       	      	       	 
    delete new_base;   	       	 
  }// fi: skill 	 	       
  else // for any other ability    	 	      
    err = current->make_new() ;
    	       		      	       	 
  if (err == SUCCESS) 	      	   	 		
  // Whether to move to the newly created item, 
  // the old selected item or the
  // end of the list ??? staying at the end of the list is desirable if
  // entering a large number of abilityies and no sort function is 
  // available. May need to change this behaviour later 
  {	       	    	      	   	 		
    cury++; 	       	       	   	 		
    show(); 	      	   	 
  }// fi: ability successfuly created	       	 	      	   
    	    	    
  return err;   
} // cursor make new   		    
    		    
    		    
char * character::dialog_title(char * field)
// returns a constructed title with Edit <ch_name>'s <field>
// calling function must delete returned string
{    	  	  
  char * new_title;
     		  
  new_title = new_str(MAX_LINE);
  if (toupper(ch_name[(strlen(ch_name)-1)]) == 'S')
    sprintf(new_title, "Change %s' %s", ch_name, field);
  else		  
    sprintf(new_title, "Change %s's %s", ch_name, field);
  return new_title;	  
}// dialog title  	  
  		  	  
int character::edit_name()
{ 		  	  
  char *new_title;	  
  char *temp_name;	  
  prompt *p;  	  
  int err = SUCCESS;
  
  // do the dialog
  say_prompt("Enter the character's new name: ");
  p = new prompt();	  	       
  temp_name = new_str(p->s); 	       
  delete p;   	      	     	       
  	      	      	     	       
  // copy the new name to ch_name if it is not a cancel
  if (!is_empty(temp_name))  	       
  {	      	      	     	       
    delete ch_name;   	     	       
    debuf(&temp_name);	     	       
    ch_name = temp_name;     	       
  	      	      	     	       
    delete file_name; 	      	       
    file_name = new_str(strlen(ch_name)+4); // 4 is the length of the extension
    strncpy(file_name, ch_name, strlen(ch_name));
    strncpy((file_name+strlen(ch_name)), ".chr", 4);
  	      	      	     	       
    show();   	      	     	       
  } else { 
    err = errUSER_CANCEL;
    if (temp_name != NULL) delete temp_name;
  } // fi

  clear_prompt();
  return err;	 
}// edit name 	      	     	       
  	      	      	     	       
int character::edit_age()   	       
{ 	      	      	     	       
  prompt *p; 	 
  int err = SUCCESS;
  	      	      	     	       	      
  say_prompt("Enter new age: ");
  p = new prompt();  
  	     	      	     	       
  if (!is_empty(p->s))	     	       
  {	     	      	     	       
    sscanf(p->s, "%d", &ch_age);      
    show();    	 	      
  } else err = errUSER_CANCEL;	   	      		       
  	       
  clear_prompt();
  delete p;  	      	     	       
  return err;
}//edit age	      		       
	   	      		       
int character::edit_race()	       
{	   	      		       
  char * temp_race;   		       
  prompt *p;       	       	       
  int err = SUCCESS;
  
  // make a working copy of the characters race
  temp_race = set_field(ch_race, 30);  
  	   	      		       
  // do the dialog		       
  say_prompt("Enter new race: ");
  p = new prompt();		       
  temp_race = debuf(p->s);	       
  delete p;	      	  	       
  	   	      	  	       
  // copy the new race to ch_race if it is not a cancel
  if (temp_race != NULL)	       
  {	  	       	  	       
    delete ch_race;    	  	       
    ch_race = temp_race;  	       
    show();	       	  	       
  } else {
    err = errUSER_CANCEL;	  	       	  	       
    delete temp_race;
  }	    

  clear_prompt();
  return err; 	 
}// edit race	       	    	       
  		       	  	       
int character::edit_sex()	       
{ 		       	  	       
  char *temp_sex;     	  	       
  prompt *p; 	 
  int err = SUCCESS;
  	     	       	 	       
  say_prompt("Enter new sex: ");
  p = new prompt();		       
  temp_sex = debuf(p->s);	 // make a copy of the sex     
  delete p;  	  		       
  if (temp_sex != NULL)		       
  {  	     	       		       
    delete ch_sex;     		       
    ch_sex = temp_sex; 		       
    show();  	 		       
  } else {   	     			       
    delete temp_sex;
    err = errUSER_CANCEL;
  } // fi: user cancel

  clear_prompt();
  return err;	      
}// edit sex	      	   	       
  		      	   	       
int character::edit_living()	       
{  		      	   	       
  char * temp_living; 	   	       
  prompt *p; 	      		       
  int err = SUCCESS;
  		 
  // make a working copy of the characters living
  temp_living = set_field(ch_living, 30);
  		      	   	       
  // do the dialog    	 	       
  say_prompt("Enter new living: ");
  p = new prompt();   		       
  temp_living = new_str(p->s);	       
  delete p;	      	 	       
	   	      	 	       
  // copy the new living to ch_living if it is not a cancel
  if (!is_empty(temp_living))	       
  {	   	  	 	       
    delete ch_living;	 	       
    debuf(&temp_living); 	       
    ch_living = temp_living;	       
    show();	 	 	       
  }	   	 	 	       
  else	   	 	 	       
  {	   	 	 	       
    delete temp_living;
    err = errUSER_CANCEL;
  }//fi	   	 
  
  clear_prompt();
  return err;	 
}// edit living	 		       
	     	 		       
int character::edit_base()	       
{	     	 		       
  prompt *p;      	     	       
  int err = SUCCESS;
  		 
  say_prompt("Enter new base: ");
  p = new prompt();	   	       
  	   	      	     	       	   
  if (!is_empty(p->s))	     	       	   
  {	   	      	     	       	   
    sscanf(p->s, "%d", &ch_base);      	   
    show();    	      	   	       
  } else err = errUSER_CANCEL;	   	      		       
  delete p;    	 
  	       
  clear_prompt();
  return err; 	 
} // edit base	 
  	      			       
int character::edit_disadv()	       
{  	       	   		       
  prompt *p;      	     	       
  int err = SUCCESS;
  	       
  log_event("begin character::edit_disadv.\n");
  // make a working copy of the characters age
  say_prompt("Enter new disadvantage maximum: ");
  p = new prompt();	   
  	       
  log_event("character::edit_disadv - prompt complete.\n");
  	       		 
  if (is_empty(p->s))	 
  {	       		 
    err = errUSER_CANCEL;  
    clear_prompt();
    say_status("Edit canceled.");
    log_event("character::edit_disadv - user cancel.\n");
  } else {    	       	      	     		   
    sscanf(p->s, "%d", &ch_max_disadvantages);
    clear_prompt();
    say_status("Maximum points from disadvantages changed to %d.", 
	       ch_max_disadvantages);
    log_event("character::edit_disadv - max disadv changed to %d.\n", 
	      ch_max_disadvantages);
    show();    	      	   	  
  }				  
       	       			  
  delete p;
  log_event("end character::edit_disadv.\n");

  return err;  			  
}// edit disadv max		  
  	       			  
int character::edit_xp()	  
{  	       			  
  prompt *p;      	     	  
  int err = SUCCESS;
  	       
  say_prompt("Enter new experience total: ");
  p = new prompt();	   
  	       	      	     		   
  if (!is_empty(p->s))	     		   
  {	       	      	     		   
    sscanf(p->s, "%d", &ch_experience);
    show();    	      	      
  } else err = errUSER_CANCEL;
  	       
  delete p;    
  clear_prompt();
  return err;	 
}// edit xp  	 
   	       	 	      
int character::bio_edit()
// edit character bio - not an object list of abilities
{  	       	    
  int err;     	 
  switch (cury-top-buf)	    
  {	       	    
    case (NAME_LINE):
    {	       	    
      err = edit_name();  
      break;   	 
    }	       	 
    case (AGE_LINE):
    {	       	 
      err = edit_age();
      break;   	 
    }	       	
    case (SEX_LINE):
    {	       	
      err = edit_sex();	     
      break;   		     
    }	       		     
    case (RACE_LINE):	     
    {	       		     
      err = edit_race();	     
      break;   		     
    }	       		     
    case (LIVING_LINE):	     
    {	       		     
      err = edit_living();	     
      break;   		     
    }	       
    case (DISADV_LINE):			    
    {	       
      err = edit_disadv();
      break;   	    			 	   
    }	       	    		      	 	   
    case (BASE_LINE):
    {	       	    
      err = edit_base();
      break;   				    
    }	       			      	    
    case (EXPERIENCE_LINE):		 	   
    {	       
      err = edit_xp();
      break;   
    }	       			      
    default:   		     	      
    {	       		      
      say_status("Bio Edit - cannot edit information on this line.");
      err = errNO_MATCH_FOUND;
      break;   	   	      			      
    }	       	       	      			  
  }//switch    	       	      
  return err;  
}//bio edit    	       				  
	       
  	       	   
void character::change_page(int page_move)
// move the specified number of pages, normaly +1 or -1
// wrap around occurs if the last or first page is reached
// returns SUCCESS or an errVALUE (see dandefs.h)
{ 	       	  
  page += page_move;
  while (page < 1)
    page += PAGES;
  while (page > PAGES)
    page -= PAGES;
  cury = cur_y_default;
  current = field_default(page);
}// change page 
  		   
int character::change_page_to(int new_page)
// change to the specified page		  
{  	       	   
  if (new_page < 1 || new_page > PAGES)
    return errOUT_OF_BOUNDS;
  if (page == new_page) return errREDUNDANT;
  page = new_page;		      
  cury = cur_y_default;		      
  current = field_default(page);      
  return SUCCESS;
}// change page to 
	       
int character::delete_current()
// delete the current ability
{			     
// abort if undeleteable     
  if (current == NULL ||  	   // nothing to delete (bio page)
      page == STAT_PAGE ||     	   // can't delete stats
      current->prev == NULL ||	   // header node    	       	       
      current->next == NULL	   // tail node    	       	 
      )		     	   
    return errINVALID;	  	      
      	   	  	      
  current = current->next;
  delete current->prev;  
  show(); // must redraw the list to move others up
  
  return SUCCESS;	   
}// delete current

int character::edit(int cmd)
{   
  int err = SUCCESS;
  switch (cmd) 	       
  {  	       	       
    // need to activate the comment on construction rutine here, when it works tk
/*  		
 case KEY_CTRL_MINUS:	     	 
    // Subtract 100 xp from current points.
    { 	      	       		     	  
      if (current != NULL) 
      {    	  
        current->change_pts(-100);
    	show();	
      }	  	
      break;  	       		  	  
    } 	      	       		  	  
      	      	       		  	  
    case KEY_CTRL_PLUS:		  	  
    // Add 100xp to current points.	 
    { 	      	       		   	 
      if (current != NULL)
      {	  	
    	current->change_pts(100);
    	show();	
      }		
      break;  	    		   	 
    } 	      	    		   	 
    case KEY_SHIFT_MINUS:
    // Subtract 10 xp from current points.
    { 		
      if (current != NULL)
      {		
      	current->change_pts(-10);
      	show();	
      }		
      break;	
    } 		
    case KEY_SHIFT_PLUS:
    // Add 10 xp to current points.
    { 		   
      if (current != NULL)
      {		   
      	current->change_pts(10);
      	show();	
      }		
      break;	
    } 		
      		
    case KEY_ALT_MINUS:		   	 
    // Subtract 1 xp from current points.
    { 	      	    		   	
      if (current != NULL)
      {		
 	current->change_pts(-1);
 	show();	
      }		
      break;	    		   	
    } 	    	    		   	
    case KEY_ALT_PLUS:		 	
    // Add 1xp to current points. 
    { 	     	    			
      if (current != NULL)
      {		
 	current->change_pts(1);
 	show();	
      }		
      break; 	    
    } 	     	    
 */         	     	    
       	   		      
    default:   	       	       	     
    {  	   
      if ( (cmd >= 33) && (cmd < 0x7A) )   // 0x7A LAST_KEY
      // plain typing - TK activate edit / insert depending on location
      {    		       
       	text_norm(); 
        say_status("Insert and edit " );
      }	   	      
      break; 	  	      	     
    }  	   	  	      	     
  }//switch
  return err;
}//edit	   	  	      
       	      	  	      
// end char.cc	      
       			      
       
