/* disadv(antage).h -- disadvantages for character
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef DISADV_H
#define DISADV_H

#include <stdio.h>

#include "ability.h"

class disadvantage : public ability
{
private:
protected:

public:
  disadvantage(disadvantage * is_after, char * new_name, int new_pts = 0);
  ~disadvantage();

  // virtual redefinitions
  void show(int x, int y, dwindow *win = NULL, int options = 0);
  void print(FILE *stdprns);
    // output to stdprn
  int pts_to_value();
  int make_new(ability* based_on = NULL);
  // returns non-zero on failure, eg user cancel
  int load(char * data);
  
  // new weapon rank methods
};
#endif
// end - disadv.h
