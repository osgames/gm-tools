/* stat.h - Statistics for auto-char
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef STAT_H
#define STAT_H

#include <stdlib.h>
#include <stdio.h>

#include "ability.h"


class statistic : public ability
{
protected:
  int maximum;
//  int cost;
  int round;      // 1 = [rounding on], 0 = [truncation]

// used by figured stats only
  virtual int get_base();

public:
  statistic(ability * is_after, char * new_name, int new_pts = 0, int new_cost = 10, int new_max = 20);
    // defults set are for the primary stat Strength
  ~statistic();

  // redefinitions of virtual bases
  int pts_to_value();
  int value_to_pts(int val);
  void set_value(int new_val); 
  void show(int x, int y, dwindow *win = NULL, int options = 0);
  void print(FILE *stdprn);
  int make_new(ability *based_on = NULL) {return 1;}; // not valid for stats
  int load(char * data) {delete data; return 1;} // not valid for stats
  
//  void figured_base(int (*newbase)(statistic * a, statistic * b = NULL, statistic * c = NULL), statistic *a,statistic *b=NULL, statistic *c=NULL);
  char * comment();
};
  
  
// miscelanious functions
// convers pts (in xp) to a printable (char*) character points
  
// BASE STAT CALCULATION: prototype int (*get_base)(statistic* a, statistic* b)
int base_pd(statistic* ch_str, statistic* unused, statistic* unused2);
// also used to calculate ed
int base_spd(statistic* ch_dex, statistic* unused, statistic* unused2);
int base_rec(statistic* ch_str, statistic* ch_con, statistic* unused2);
// also used for mana rec, str==int, con==dis
int base_end(statistic* ch_con, statistic* unused, statistic* unused2);
// also used for mana: con==dis
int base_stn(statistic* ch_bod, statistic* ch_str, statistic* ch_con);


#endif
// end STAT.H
