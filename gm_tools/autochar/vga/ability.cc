// ABILITY.CPP - virtual base class for char(acter) abilities
#include <stdlib.h>
#include <stdio.h>
		   
#include "ability.h"
#include "sat.h"   
#include "cost.h"  
#include "assert.h"


void ability::save(FILE * out)
{		   
  if (name != NULL)
  {
    fprintf(out, "%d %d \"%s\"\n", pts, cost, debuf(name));
  }	   
}// save   
	   
char* ability::new_prn_pts(int num)
// convers pts (in xp) to a printable (char*) character points
	   
{	   
  char * buff = new char[40];
  if (!(num < 0))
    sprintf(buff, "%d.%d", num/10, num%10);
  else	   
    sprintf(buff, "-%d.%d", -num/10, -num%10);
  return buff;
}// statistic:: print points
	   
char* ability::new_prn_pts()
// convers pts (in xp) to a printable (char*) character points
{	   
  char * buff = new char[40];
  if (!(pts < 0))
    sprintf(buff, "%d.%d", pts/10, pts%10);
  else	   
    sprintf(buff, "-%d.%d", -pts/10, -pts%10);
  return buff;
}// statistic:: print points
	   
inline void ability::change_pts(int amt)
{	   
  pts += amt;
}	   
	   
inline int ability::get_base()
// returns a constant for some derived classes, or a dependant function
{	   
  return 0; // default
}	   
	   
	   
inline int ability::get_pts()
{	   
  return pts;
}	   
	   
ability::ability(ability * is_after, char * new_name, int new_pts, int new_cost)
{	   
	if( is_after != NULL )
	{  
	   	prev = is_after;
	   	next = is_after->next;
	   
      prev->next = this;
      if (next != NULL) next->prev = this;
	}  
	else // this is the 1st line
	{  
	   	prev = next = NULL;
	}  
	   
  if (new_name != NULL)
    name = new_str(new_name);
  else	   
    name = NULL;  
  pts = new_pts;
  cost = new_cost;
}// ability constructor
	   
ability::~ability()
{	   
  if (prev != NULL) prev->next = next;
  if (next != NULL) next->prev = prev;
  if (name != NULL) delete name;
}	   
	   
int ability::make_new(ability* based_on)
{	   
  gl_write(8,8, " make new ");
  return 1;
}	   
	   
int ability::pts_to_value()
// defult behaviour, overide for stat maximums, bases, etc
{	   
  if (pts < 0) return -1;
  assert (cost > 0);
  return (int) (((float) pts / (float) cost)) + get_base();
}//pts_to_value
	   
void ability::change_value(int amt)
{	   
  assert (amt); // amt is non-zero
	   
  if (name == NULL) return; // dummy node;
	   
  int value, bs, new_val;
  value = pts_to_value();
	   
  if ((value == 0) && (amt < 0)) return; // dont go < 0 value
	   
  new_val = value + amt;
	   
  if (value <= 1)
  {	   
  bs = get_base();
  pts = (new_val - bs) * cost;
  }	   
  else	   
  {	   
    if (amt > 0)
    {	   
      while (pts_to_value() < new_val)
        change_pts(1);
    }	   
    else   
    {	   
      while (pts_to_value() > (new_val-1))
        change_pts(-1);
      change_pts(1);  // the overshoot is so we get _all_ the points back
    }	   
  }	   
}//change value
	   
int ability::sum()
{	   
  int count;
  ability * look;
	   
  look = this;
	   
  count = 0;
  while (look != NULL)
  {	   
    count += look->get_pts();
    look = look->next;
  }	   
  return count;
}// sum combat
	   
// end - ABILITY.CPP
