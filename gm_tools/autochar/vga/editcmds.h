// EDITCMDS.H -- edit command defs in Tiny Editor
// Copyright 1995, Martin L. Rinehart

#define EXIT   	     	1000
#define NOP	     	1999
#define CURSOR_UP	1001
#define CURSOR_DN    	1002
#define CURSOR_LF    	1003
#define CURSOR_RT    	1004

#define GO_PGUP         1011
#define GO_PGDN         1012
#define GO_BOF          1013
#define GO_EOF		      1014
#define GO_HOME         1015
#define GO_END          1016
#define WORD_LF         1017
#define WORD_RT         1018

#define DEL_CHAR        1031
#define DEL_PREV_CHAR   1032
#define DEL_WORD        1033
#define DEL_LINE        1034

#define DO_ENTER        1041

#define TOGGLE_SCREEN   1801
#define TOGGLE_INSERT   1802

#define WIN_MOVE_UP     1901
#define WIN_MOVE_DN     1902
#define WIN_MOVE_LF     1903
#define WIN_MOVE_RT     1904

#define WIN_SIZE_UP     1911
#define WIN_SIZE_DN     1912
#define WIN_SIZE_LF     1913
#define WIN_SIZE_RT     1914

// end of EDITCMDS.H
