// STAT.CPP - Statistics definitions for auto-char
// Daniel Vale Jan 2001

#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <vga.h>
#include <vgagl.h>

#include "stat.h"
#include "sat.h"

/*
void statistic::figured_base(int (*newbase)(statistic * a, statistic * b, statistic * c),
                             statistic *a,statistic *b, statistic * c)
// stores a pointer to a function for calculating the base
{
  get_base = newbase;
  base_a = a;
  base_b = b;
  base_c = c;
}//figured base

*/
statistic::statistic(ability * is_after, char* new_name, int new_pts, int new_cost,
                     int new_max) :
  ability(is_after, new_name, new_pts)
{
  cost = new_cost;
  maximum = new_max;
}

statistic::~statistic()
{
}

int statistic::get_base()
// default for stats
{
  return 10;
}

int statistic::pts_to_value()
// convert point to a value given points spent, cost, base and maximum
// uses mathimatical rounding, inapropriate for speed and primary stats, which
// must be paid for in full.
{			    
  int temp;		    
			    
  assert(cost > 0);	    
  temp = (int) ((float) (pts + get_base() * cost) / (float) cost);  
  			    
  if (temp > maximum)	    
  {			    
    temp = (pts - ((maximum - get_base())*cost));
    temp = (int) ((float) temp / (float) (cost*2));
    temp += maximum;
  }
  return temp;
}

void statistic::show(int x, int y)
{ 
  gl_printf(x*8, y*8, "%3d", pts_to_value()); x += 3;
  gl_printf(x*8, y*8, " %-14s", name);    x += 14; 	     
  gl_printf(x*8, y*8, "%6d", get_base()); x+=6;
  
  gl_printf(x*8, y*8, "%6d", cost); x+=6;
  gl_printf(x*8, y*8, "%5d", maximum); x+=5;	     
  gl_printf(x*8, y*8, "%5d\n", pts); x+=5;	     
}		      	  			     
		      	  
void statistic::print(FILE *stdprn)
{		      	  
  fprintf(stdprn, "\r\n");
  fprintf(stdprn, "%3d", pts_to_value());
  fprintf(stdprn, " %-14s", name);
  fprintf(stdprn, "%6d", get_base());
  fprintf(stdprn, "%6d", cost);
  fprintf(stdprn, "%5d", maximum);
			  
  fprintf(stdprn, "%5d", pts);
}			  
///////////////////// miscellanious functions
			  
// end STAT.CPP		  
