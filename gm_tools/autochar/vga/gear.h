// gear.h -- gear for character
#ifndef gear_H 
#define gear_H 
	       
#include <stdio.h>
	       
class gear : public disadvantage
// gears are built like disadvantages, once off payment.
{	       
private:       
protected:     
	       
public:	       
  gear(gear* is_after, char * new_name, int new_pts = 0);
  ~gear();     
	       
  // virtual redefinitions
  void show(int x, int y);
  void print(FILE *stdprn);
    // output to stdprn
  int make_new(ability* based_on = NULL);
    // returns non-zero on failure, eg user cancel
  int load(char * data);
};//gear       
	       
	       
#endif	       
// end - gear.h
