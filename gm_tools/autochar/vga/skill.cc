// skill.cpp -- skills for char(acter)

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "skill.h"
#include "ability.h"
#include "dan_keyboard_buffer.h"
#include "cost.h"
#include "vga_prompt.h"

skill::skill(ability *isafter, char *new_name, int new_cost, ability * based_on, int new_pts, char* new_find) :
  ability(isafter, new_name, new_pts, new_cost)
{
  base_stat = based_on;
  find_base = new_str(new_find);
}

skill::~skill()
{
}

int skill::make_new()
// use make_new(ability*), pass a NULL for general skills
{
  assert(0); // this function is obsolete

  skill * new_skill;
  char* new_name;
  vga_prompt *p;
  
  text_norm();
  blank_line(STATUS_LINE); 	     
  gl_write(8, STATUS_LINE * 8, "Enter the name of the new skill");
  p = new vga_prompt(8, PROMPT_LINE * 8);
  new_name = new_str(p->s);
  delete p;   	       	 

  if (this->prev != NULL)
    new_skill = new skill(this->prev, new_name, 20, NULL);
  else
    new_skill = new skill(this, new_name, 20, NULL);

  delete new_name;
  return 0;
}      
       				 
void skill::set_base(ability * based_on)
  // changes/sets the base	 
{      				 
  base_stat = based_on;		 
  fprintf(stderr, "skill base set: %d %s %s %d %d\n",	
          pts_to_value(), name,	 
          base_stat->name, cost, pts);
}				 
				 
			
int skill::make_new(ability * based_on)
// make a new skill based on a stat
{			
  skill * new_skill;	
  char* new_name;		     
  int new_cost;			     
  vga_prompt *p;		     
				     
       	     	   	 	     
  blank_line(STATUS_LINE);	     
  gl_write(8, STATUS_LINE*8, "Enter the name of the new skill");
  p = new vga_prompt(1, PROMPT_LINE);
  new_name = new_str(p->s);  	     
  if (is_empty(new_name))    	     
  {	   	   	     	     
    delete new_name;	     	     
    return 1;// test for no input == user cancel
  }	   	   	     	     
  delete p;	   	     	     
  blank_line(STATUS_LINE);   	     
  gl_write(8, STATUS_LINE*8, "Is this a background skill?");
  p = new vga_prompt(1, PROMPT_LINE);		       
  if (is_empty(p->s)) 		     		       
  {				     		       
    delete p;			     		       
    return 1;			     		       
  }	     			     		       
  if (toupper(p->s[0]) == 'Y')	     		       
    new_cost = cBSK;		     		       
  else				     		       
    new_cost = cSKL;		     		       
  delete p;   	    		     		       
  	  			     		       
  if (this->prev != NULL)	     		       
    // isert after previous skill    		       
    new_skill = new skill(this->prev, new_name, new_cost, based_on);
  else	  			     		       
    // insert first skill in list    		       
    new_skill = new skill(this, new_name, new_cost, based_on);
	  					       
  delete new_name;   // clean up		       
  return 0;					       
}// make new					       
	  					       
int skill::load(char * data)			       
{ 	  					       
  skill *new_skill;				       
  char * new_name;				       
  char * base_data;				       
  int new_pts;					       
  int new_cost;					       
  	  					       
  // get pts					       
  sscanf(data, "%d%d", &new_pts, &new_cost);	       
  (void) strtok(data, "\"");  // find the begining of new_name
  new_name = new_str(strtok(NULL, "\"")); // get new_name
  base_data = new_str(strtok(NULL, "\"")); // get base_data
  fprintf(stderr, "base %s\n", base_data);
  delete data;					       
	  					       
  // prepare base_data for stat search		       
  debuf(&base_data);    // remove whitespace	       
  str_upp(base_data);    // standardis in upper case   
  base_data[3] = '\0';  // shorten to three letter code
	  					       
  // NB: base_stat is set from character after the load is compleated
	  					       
  if (this->prev == NULL)			       
  // this is the dummy head node		       
    new_skill = new skill(this, new_name, new_cost, NULL, new_pts, base_data);
  else	  					       
    new_skill = new skill((skill*)prev, new_name, new_cost, NULL, new_pts, base_data);
	  					       
  delete base_data;				       
  delete new_name; // is reallocated for in constructor
  fprintf(stderr, "skill loaded: %d %s %d %d \n",     
	  new_skill->pts_to_value(), new_skill->name, 
	  new_skill->cost,  new_skill->pts);
  return 0;					       
}//load						       
						       
int skill::pts_to_value()			       
{						       
#ifdef HERO					       
  int temp = 0;	  	 		       	       
  int stat_base;				       
  int base;					       
  						       
  if (pts < 10) return 0;			       		   
  if (pts < 30) return 8;			       		   
		 				       
  if (base_stat != NULL)			       
  {		 				       
    stat_base = 9 + (int) ((float) base_stat->pts_to_value() / 5.0 + 0.5);
    base = ((stat_base > 11) ? stat_base : 11);	       
  }		 				       
  else base = 11;				       
  	      	  	 			       		   
  temp = base + ((pts - 30) / cost);		       
  return temp;	 				       
  		 				       
#else 		 				       
  int temp;	 				       
  		 				       
  if (pts < 10) return 0;			       
  temp = 30;	 				       
  temp += base_stat->pts_to_value(); 		       
  temp += (pts - 10) / cost;	     		       
#endif  	 		     		       
}// pts_to_value - hero's version    		       
      				     		       
void skill::show(int x, int y)	     		       
{     				     		       
  if (name != NULL)		     
    gl_printf(8*x, 8*y, "%3d  %14s%12s%4d%5d", 
	      pts_to_value(), name, base_stat->name, cost, pts);
  else
    gl_printf(8*x, 8*y, "---End of Skills---");
} // skill show
      		 
void skill::print(FILE *stdprn)
{     		 
  char *show_pts, *show_cost;
      		 
  if (name != NULL)
    fprintf(stdprn, "\r\n%3d  %s%d  %d", pts_to_value(), name, cost, pts);
  else
    fprintf(stdprn, "\r\n---End of Skills---");
}//print	 
      		 
void skill::save(FILE * out)
      		 
{     		 
  if (name != NULL)
  {   		 
    ability::save(out);
    fseek(out, -1, SEEK_CUR); // go back 1 byte, before \n
    if (base_stat != NULL)
      fprintf(out, " %s\n", base_stat->name);
    else	     
      fprintf(out, " GENERAL\n");
  }

}// save
  
#ifdef HERO
void skill::change_value(int amt)
// overide virtual base function to go from stat based to 8-
{      	   
  int value, new_val;
  	   
  assert (amt); // amt is non-zero
  if (name == NULL) return; // skip dummy node;
    	   	    
  value = pts_to_value(); // don't want to keep recalculating it here
    	   
  if ((value == 0) && (amt < 0)) return; // dont go < 0 value
  
  new_val = value + amt; // change the value by the amount req 
				    
  if ((amt < 0) && (pts >= 30) && (pts < (30 + cost)))
  // this is the point of overiding the base class, we don't want skills to 
  // stop at stat based, we want to go down to fam, even though this is 
  // likely less than indicated by amt 
  {  	     	    			  	   
    new_val = 8;    			  
    change_pts(-20); // we need to dump at least this much   
  }  	       				      
      	       				      
  if (amt > 0) 				      
  // go up 
  { 	     
    while (pts_to_value() < new_val)
      change_pts(1);
  } 	     
  else     
  // go down 
  { 	     
    while (pts_to_value() > (new_val-1))
      change_pts(-1);
    change_pts(1);  // the overshoot is so we get _all_ the points back
  } 	     
}//change value
#endif	     
     	     
     	     
// end - skill.cpp
	   
	   
