// STAT.H - Statistics for auto-char
#ifndef STAT_H
#define STAT_H

#include <stdlib.h>
#include <stdio.h>

#include "ability.h"


class statistic : public ability
{
protected:
  int maximum;
//  int cost;
  int round;      // 1 = [rounding on], 0 = [truncation]

// used by figured stats only
  virtual int get_base();

public:
  statistic(ability * is_after, char * new_name, int new_pts = 0, int new_cost = 10, int new_max = 20);
    // defults set are for the primary stat Strength
  ~statistic();

  // redefinitions of virtual bases
  int pts_to_value();
  void show(int x, int y);
  virtual void print(FILE *stdprn);
  int make_new(ability *based_on = NULL) {return 1;}; // not valid for stats
  int load(char * data) {delete data; return 1;} // not valid for stats

//  void figured_base(int (*newbase)(statistic * a, statistic * b = NULL, statistic * c = NULL), statistic *a,statistic *b=NULL, statistic *c=NULL);
  char * comment();
};


// miscelanious functions
// convers pts (in xp) to a printable (char*) character points

// BASE STAT CALCULATION: prototype int (*get_base)(statistic* a, statistic* b)
int base_pd(statistic* ch_str, statistic* unused, statistic* unused2);
// also used to calculate ed
int base_spd(statistic* ch_dex, statistic* unused, statistic* unused2);
int base_rec(statistic* ch_str, statistic* ch_con, statistic* unused2);
// also used for mana rec, str==int, con==dis
int base_end(statistic* ch_con, statistic* unused, statistic* unused2);
// also used for mana: con==dis
int base_stn(statistic* ch_bod, statistic* ch_str, statistic* ch_con);


#endif
// end STAT.H
