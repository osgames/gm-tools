// perk.h -- perks for char(acyter)
#ifndef PERK_H
#define PERK_H

#include <stdio.h>

class perk : public disadvantage
// perks are built like disadvantages, once off payment.
// perks built like skills should be included in the characters skill list
// perks like find weekness (skill like with cost 50) wont hadle yet - code tk
{
private:
protected:

public:
  perk(perk* is_after, char * new_name, int new_pts = 0);
  ~perk();

  // virtual redefinitions
  virtual void show(int x, int y);
  virtual void print(FILE *stdprn);
    // output to stdprn
  virtual int make_new(ability* based_on = NULL);
    // returns non-zero on failure, eg user cancel
  virtual int load(char * data);
};//perk


#endif
// end - perk.h