// cost.h - costs of abilities for char(acter)
      
#ifndef COST_H
#define COST_H

#define HERO // comment this out for Frame rules 
// NB: costs are in xp (= 1/10 HERO cp)

#ifdef HERO    
/////////////////////////// Hero costs ////////////////////// 
#define cSTR 10
#define cDEX 30
#define cCON 20
#define cBOD 20
#define cINT 10
#define cDIS 20
#define cPRE 10
#define cCOM  5
      	      
#define cPD  10		   
#define cED  10 
#define cSPD 100 
#define cEND  5	       	   
#define cSTN 10	
#define cREC 20
#define cMAN  5
#define cMRC  20
#define cRUN 20 	   	   
#define cSWM 10 	   	   
      	       	   	   
#define cPER 30 // Perception
#define cSKL 20 // cost to increase most skills
#define cBSK 10 // cost to increase background skills
      	       	    	    
// maximums    	    	    
#define mPD  8	    	    
#define mED  8	    
#define mSPD 4 
#define mEND 50    	    
#define mSTN 50
#define mREC 10
#define mMAN 50
#define mMRC 10
#define mRUN 10	    	    
#define mSWM  5    	    


#else	       	    
///////////////////// Frame costs ////////////////////////////
	      	    
#define cSTR 10	    
#define cDEX 30	    
#define cCON 20	    
#define cBOD 20
#define cINT 10
#define cTECH 10 
#define cDIS 20
#define cPRE 10
#define cCOM  5
      	      
#define cPD  5		   
#define cED  5
#define cMD  5
#define cSPD 5 // Rediness 
#define cEND  5	       	   
#define cSTN 10	   	   
#define cMAN  5	   	   
#define cRUN 5	   	   
#define cSWM 2	   	   
      		   	   
#define cPER 2 // Perception
#define cSKL 2 // cost to increase most skills
#define cBSK 1 // cost to increase background skills
      		   	    
// maximums	   	    
#define mPD  10	   	    
#define mED  10	   
#define mMD  10	
#define mSPD 100 // reaction
#define mEND 100   	    
#define mSTN 100   	    
#define mMAN 100   	    
#define mRUN 30	   	    
#define mSWM  15   	    
      		   
#endif // Hero / Frame
#endif // already included	       	      	    
// end - cost.h	   	    
      		   
