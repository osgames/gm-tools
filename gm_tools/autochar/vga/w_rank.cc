// w_rank.cpp - weapon ranks for char(acter)
#include <stdlib.h>
#include <stdio.h>
#include <string.h> 
#include <vga.h>
#include <vgagl.h>
#include "vga_prompt.h"
		  
#include "w_rank.h"
#include "assert.h"
#include "sat.h"  


weapon_rank::weapon_rank(weapon_rank * is_after, char * new_name, int new_cost, int new_pts) :
  ability(is_after, new_name, new_pts, new_cost)
{
// new weapon ranks created dynamicly on as needed basis.
}

weapon_rank::~weapon_rank()
{
  if (name != NULL) delete name;
}// weapon rank destructor

void weapon_rank::show(int x, int y)
{

  if (name == NULL)
    // dummy end node for inserting new last line
    gl_write(x*8, y*8, "---End of Weapons---");
  else		       
    gl_printf(x*8, y*8, "%5d  %-20s%3d%5d", (pts_to_value()), name, cost, pts);
}// show

void weapon_rank::print(FILE *stdprn)
{
  if (name == NULL) // dummy end node for inserting new last line
    fprintf(stdprn, "\r\n---End of Weapons---");
  else
    fprintf(stdprn, "\r\n%5d  %-20s%3d%5d", (pts_to_value()), name, cost, pts);
}// print

int weapon_rank::make_new(ability* based_on)
// creates a new weapon skill and iserts before current.
{
  weapon_rank * new_weapon;
  char * temp;
  char * new_name;;
  int new_cost;	  	     
  vga_prompt *p;	       	     
      	      	  	     
// get a name for the weapon skill
  gl_write(8,8,"Enter the weapon group");
  p = new vga_prompt(8, PROMPT_LINE);  
  new_name = new_str(p->s);	   
  delete p;	  		   
				   
  if (is_empty(new_name)) return 1;// test for no input == user cancel
  debuf(&new_name);		   
				   
// get cost			   
  gl_write(8,8,"Enter the weapon cost.");
  p = new vga_prompt(8, PROMPT_LINE);
  if (is_empty(temp))	    
  {			    
    delete p;       	    
    return 1;
  }	   
	   
  sscanf(p->s, "%d", &new_cost);
  delete p;

// create the new weapon skill
  if (prev == NULL)
  // this is the first to be created
  {
    new_weapon = new weapon_rank(this, new_name, new_cost);
  }
  else
    new_weapon = new weapon_rank((weapon_rank*)prev, new_name, new_cost);

  delete new_name;
  return 0;
}//make new

void weapon_rank::save(FILE*out)

{
  ability::save(out);
}

int weapon_rank::load(char * data)
{
  weapon_rank *new_weapon_rank;
  char *new_name, *tempstr;
  int new_pts;
  int new_cost;

  sscanf(data, "%d%d", &new_pts, &new_cost); // get points spent and cost
  tempstr = new_str(data);                   // a tempory is used as strtok must
                                             // be able to change the value of it
  strtok(tempstr, "\"");                     // move to the first quote
  new_name = new_str(strtok(NULL, "\"")); // copy to the second quote
  delete tempstr;                            // done with the tempory
			    
  if (this->prev == NULL)   
  // called from the dummy head node
    new_weapon_rank = new weapon_rank(this, new_name, new_cost, new_pts);
  else			    
    new_weapon_rank = new weapon_rank((weapon_rank*)prev, new_name, new_cost, new_pts);
			    
  delete new_name; // is reallocated for in constructor
  fprintf(stderr, "weapon rank loaded: %d %s %d %d \n",     
	  new_weapon_rank->pts_to_value(), new_weapon_rank->name, 
	  new_weapon_rank->cost,  new_weapon_rank->pts);
  // ensure data is deallocated elsewhere tk
  return 0;
} // load

// end w_rank.cpp
