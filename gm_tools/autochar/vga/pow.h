// power.h -- powers for char(acyter)
#ifndef power_H
#define power_H

#include <stdio.h>

class power : public disadvantage
{
private:
protected:

public:
  power(power* is_after, char * new_name, int new_pts = 0);
  ~power();

  // virtual redefinitions
  void show(int x, int y);
  void print(FILE *stdprn);
    // output to stdprn
  int make_new(ability* based_on = NULL);
   // returns non-zero on failure, eg user cancel
  int load(char * data);
};//power


#endif
// end - power.h
