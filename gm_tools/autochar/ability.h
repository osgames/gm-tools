/* ability.h - virtual base class for character abilities
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#ifndef ABILITY_H
#define ABILITY_H

#include <stdlib.h>
#include <stdio.h> 

#include "common.h"
#include "sat.h"

class ability			   
{				   
private:			   
				   
protected:			   
  int pts; // experience point spent in ability
  int cost; // in experience points
				   
  virtual int get_base(); // returns a constant for some derived classes
				   
public:				   
  char * name;			   
  ability * next;		   
  ability * prev;		   
				   
  ability(ability * after_line, const char * new_name, int new_pts = 0, int new_cost = 10);
  ~ability();			   
				   
  int sum();			   
    // sums up abilities from this one to the terminating NULL
				   
  virtual int make_new(ability * based_on = NULL);
				   
  virtual void change_pts(int amt);
  virtual int get_pts();	   
				   
  virtual char* new_prn_pts();	   
  virtual char* new_prn_pts(int num);
    // dynamicly allocates char*   
				   
  virtual void change_value(int amt);
  virtual int pts_to_value();
  virtual int value_to_pts(int val);
  virtual void set_value(int new_val);
  int get_cost() { return cost; }
  virtual void save(FILE * out);   
  virtual int edit() {dcur_move(1, 1); dwrite("Edit ability            " ); return SUCCESS;}
    // load must delete char * data
	       	       	       	   
  // pure virtual methods	   
  virtual void show(int x, int y, dwindow *win = NULL, int options = 0) = 0;
  virtual void print(FILE *stdprn) = 0;
  virtual int load(char * data) = 0;
};//ability			   
				   
#endif

// end - ability.h
   
