/* disadv.cc -- disadvantages for character
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

		       
#include <stdlib.h>    
#include <stdio.h>     
#include <string.h>     	       	   
	     	       		   
#include "disadv.h"
#include "common.h"
#include "sat.h"  
#include "prompt.h"
#include "err_log.h"
		   
disadvantage::disadvantage(disadvantage * is_after, char * new_name, int new_pts) :
  ability(is_after, new_name, new_pts)
{	     
}	     
	     
disadvantage::~disadvantage()
{	     
  if (prev != NULL) prev->next = next;
  if (next != NULL) next->prev = prev;
  if (name != NULL) {
    delete name;
    name = NULL;
  }
  
}	     
	     
int disadvantage::load(char * data)
{	     
  disadvantage * new_disadvantage;
  char * new_name;
  int new_pts;
  int new_cost; // place holder only
	     
  sscanf(data, "%d%d", &new_pts, &new_cost);
  strtok(data, "\"");
  new_name = new_str(strtok(NULL, "\""));
  delete data;
	    
	    
  if (this->prev == NULL)
  // this is the dummy head node
    new_disadvantage = new disadvantage(this, new_name, new_pts);
  else	    
    new_disadvantage = new disadvantage((disadvantage*)prev, new_name, new_pts);
	    
  delete new_name; // only a copy is taken by disadv constructor
  log_event("disadvantage loaded: %d %s %d\n",
  	  new_disadvantage->pts_to_value(), new_disadvantage->name,
	  new_disadvantage->pts);
  return 0;					     
						     
}//load						     
						     
// virtual redefinitions			     
int disadvantage::pts_to_value()		     
{						     
  return pts;					     
}						     

void disadvantage::show(int x, int y, dwindow *win, int options)
{						  
  if (name == NULL)				  
  {						  
    dcur_move(x, y, win);				  
    dwrite(win, "----End of Disadvantages---");	  
  } else {	       				  
    dcur_move(x, y, win);				  
    dwrite(win, "%5d  %s", pts_to_value(), name);	  
  }//fi	     					  
  						  
}//show	  					  
      	  					  
void disadvantage::print(FILE *stdprn)
{     	  
  if (name == NULL)
    fprintf(stdprn, "\r\n---End of Disadvantages---");
  else	  
    fprintf(stdprn, "\r\n%5d  %s", pts_to_value(), name);
}// print 
      	  
int disadvantage::make_new(ability* based_on)
// returns non-zero on failure, eg user cancel
{     
  char * new_name;
  int new_value; 
  disadvantage * new_disadvantage;
  prompt *p;
  	   
// get a name
  text_norm();
  say_prompt("Enter a description of the disadvantage: ");
  p = new prompt();				       
  new_name = new_str(p->s);			       
  delete p;   	       	 			       
  if (is_empty(new_name))
  {
    clear_prompt();
    say_status("New disadvantage cancelled.");
    return 1;		       		      
  }
  
  	       	       				       
// get value
  say_prompt("Enter the value of the disadvantage: "); 
  p = new prompt();
  if (is_empty(p->s))
  {
    clear_prompt();
    say_status("New disadvantage cancelled.");
    return 1;		       		      
  }
  sscanf(p->s, "%d", &new_value);  
  delete p; 			   
  	    		   	   
// record new disadvantage 	   
  if (this->prev == NULL)  	   
  // first diasadvantage in list   
    new_disadvantage = new disadvantage(this, new_name, new_value);
  else	    		   	   
    new_disadvantage = new disadvantage((disadvantage*)this->prev, new_name, new_value);
	   		   	   
  return 0;		   	   
}//make new		   	   
			   	   
// end disadv.cc
				   
