/* gtime.cc
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#include <curses.h>

#include "gtime.h"

gtime::gtime(gtime& source)
// initialise a gtime from another gtime
{
  year = source.year;
  day = source.day;
  hour = source.hour;
  minute = source.minute;
  second = source.second;
}// constructor (copy)

gtime::gtime(int new_year, int new_day, int new_hour, int new_minute, float new_second)
  // initialise a gtime from all the bits tk
{
  year = new_year;
  day = new_day;
  hour = new_hour;
  minute = new_minute;
  second = new_second;
}// constructor (all the bits)

void gtime::show()
// show the time
{
  // day/month/year
  if (day % 28 == 0)
    printw("%d/%d/%d ", 28, (day / 28), year);
  else	  
    printw("%d/%d/%d ", (day % 28), (day / 28)+1, year);
  //hour:minute	 
  if (hour < 10) 
    printw("0"); 
  printw("%d:", hour);
  if (minute < 10)
    printw("0"); 
  printw("%d", minute);
		 
  //seconds	 
  // tk textcolor(DARKGRAY);
  printw(":");	 
  if (second < 10)
    printw("0");  // padding zero
  printw("%.2f", second);
  // tk textcolor(LIGHTGRAY);
}// show	 
		 
void gtime::normalise()
// normalise time, eg 63 minutes becomes 1 hour 3 minutes
// works only for time which has been added to not subtracted from.
{		 
		 
  while (second >= 60)
  {		 
    minute++;	 
    second -= 60;
  }	
/*
  while (second < 0)
  {	
    minute--;
    second += 60;
  }	      
  while (minute < 0)
  {	      
    minute++;
    hour -= 1;
  }
 */ 
  hour += minute / 60;
  minute %= 60;
  day += hour / 24;
  hour %= 24;
  year += day / 364;
  day %= 364;
  if (day == 0) // there is no zeroth day in a year, but there is a 365th
  {	
    day = 364;
    year -= 1;
  }//fi	
  // years are limited only by integer math limit
}//normalise 
	     
gtime gtime::advanced(float amount)
// returns a gtime which is _this + the amount indicated in the parameters
{	     
  gtime t(*this);
	     
  t.second += amount;
  t.normalise();
  return t;  
}//advanced (float)
	     
gtime gtime::advanced(int amount, int unit)
// returns a gtime which is _this + the amount indicated in the parameters
// usage: latter = now.advanced(10, YEARS);
{	     
  gtime t(*this);
	     
  switch (unit)
  {	     
    case(YEARS):
    {	     
      t.year += amount;
      break; 
    }//years 
    case(DAYS):
    {	     
      t.day += amount;
      break; 
    }//days  
    case(HOURS):
    {	     
      t.hour += amount;
      break; 
    }//hours 
    case(MINUTES):
    {	     
      t.minute += amount;
      break; 
    }//minutes
    case(SECONDS):
    {	     
      t.second += (float) amount;
      break; 
    }//seconds
  }//switch  
	     
  t.normalise(); // normalise time, eg 63 minutes becomes 1 hour 3 minutes
	     
  return t;  
}// advanced(int int)
	     
	     
gtime gtime::operator+(gtime t)
// addition of times (the resultant is normalized)
{	
  gtime n(t); // new time instance to be returned
	
  n.year += year ;
  n.day += day;
  n.hour += hour;
  n.minute += minute;
  n.second += second;
  n.normalise();
  return n;
}// operator+
	
gtime gtime::operator-(gtime t)
// subtraction of times (the resultant is normalized)
{	
  gtime n;    // new time instance to be returned
  n.year = year - t.year;
  n.day = day - t.day;
  n.hour = hour - t.hour;
  n.minute = minute - t.minute;
  n.second = second - t.second;
  n.normalise();
  return n;
}// operator-
	
int gtime::operator<(gtime t)
// time comparison
{	
  if (year == t.year)
    if (day == t.day)
      if (hour == t.hour)
        if (minute == t.minute)
          return (second < t.second);
        else
          return (minute < t.minute);
      else
        return (hour < t.hour);
    else
      return (day < t.day);
  else	
    return (year < t.year);
}// operator<
// end - gtime.cpp
	
