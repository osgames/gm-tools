/* actdef.h - activity definitions / command codes
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#ifndef ACTDEF_H
#define ACTDEF_H

// special 0 - 999
#define is_special(x) (((x) >= 0) && ((x) <= 999))
#define NO_CHANGE 0
#define NOT_UNDERSTOOD 1
#define DUMMY_NODE 2

// comands 1000 - 2999
#define is_command(x) (((x) >= 1000) && ((x) <= 2999))
#define NO_COMMAND 1000
#define cQUIT 1001


// actions 3000 - 5999
#define is_action(x) (((x) >= 3000) && ((x) <= 5999))
#define REST 3000
#define MOVE_NORTH 3001
#define MOVE_EAST 3002
#define MOVE_SOUTH 3003
#define MOVE_WEST 3004
#define ATTACK 3005

// events 6000 - 9999
#define is_event(x) (((x) >= 6000) && ((x) <= 9999))
#define RANDOM_MONSTER_CHECK 6000
#define RANDOM_MONSTER_ENCOUNTER 6001


#endif
// end - actdef.h
