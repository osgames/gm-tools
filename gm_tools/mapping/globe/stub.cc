/* test.cpp - a testbed for prompt()
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#include <curses.h>
  	     	   
#include "prompt.h"
  
void main()  	   
{    	    
  prompt * name;
  	    
  initscr();   
  start_color(); 
  	       	       
  nonl();      	       
  cbreak();    	       
  noecho();    	       
  keypad(stdscr, TRUE);	       	 
       	       	       		 
  printw("Hello, what is you're name: ");
  name = new prompt();	   	 
  name->psetcolour(0x41);	 
  printw("\nHello %s.\n", name->s);
  (void) getch();		       
  delete name;
  endwin(); // close ncurses
} 	
  	
  	
// end - test.cpp
  	
