/* powers.cc - power editor and test bed for the power object
 *
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#include <stdio.h>
#include <time.h>
#include <string.h>

#include "prompt.h"
#include "status_line.h"
#include "sat.h"
#include "dandefs.h"
#include "danio.h"
#include "menu.h"
#include "err_log.h"

#define TITLE_LINE 1
#define PROMPT_COLOUR 7
#define PROMPT_LEN 20
#define xoMAIN_MENU 23

// window co-ordinates for the menu
#define MENU_WIN_X1 40
#define MENU_WIN_Y1 3
#define MENU_WIN_X2 79
#define MENU_WIN_Y2 20

// command list - used in conjunction with menu.h
//
//
//
//
// all application commands are 2000 +
#define cmdQUIT 2000


int filter_cmd(int usr_cmd)
// dissalow commands by context here
{
      return usr_cmd;
}//filter cmd

void cleanup_prompt(prompt *main_prompt, char *question)
// clear any information remaining from previous entries
{
  setpcolour(PROMPT_COLOUR);            // make sure the colour is right
  dcur_move(1, PROMPT_LINE);
  dwrite(BLANK_LINE);                                  // clear garbage
  main_prompt->setpstr(NULL);           // get rid of the old input
  main_prompt->redraw();                // redraw the input window
  dcur_move(1, PROMPT_LINE);
  dwrite(question);                     // redraw the question
} // cleanup prompt

void handle_event(int cmd,  menu_el *main_menuint, char **question, int& working, prompt *main_prompt)
{
  switch (cmd)
  {
        case (cmdQUIT): working = FALSE; break;
        default: say_status("That option is not known."); break;
  }
}//heandle event

void user_input(char **question, prompt *main_prompt,
                                                menu_el *main_menu, int& working)
// Track and handle user input.
// Keeps track of the prompt and looks for input in the form of a terminated
// prompt (cmd == CMD_QUIT) or a hot-key.
// Input is then passed to an interpreter which converts raw input to a
// mnemonic command. Commands are then compared to the context to dissalow
// improper commands (eg go up when there is no way up). Finally commands are
// passed to the handler.
{
  int usr_cmd;    // a user selection
  int prompt_cmd; // an internal control command from the prompt, used to
                  // detect prompt termination, usually by pressing <Enter>

  prompt_cmd = main_prompt->get_ed_cmd(); // look for input
  if (prompt_cmd != NO_COMMAND) // there was a keystroke
  {
    log_event("Prompt command: %d\n", prompt_cmd);
    log_event("Prompt key: %d\n", main_prompt->key);
    main_prompt->do_ed_cmd();        // make the prompt work

    clear_status_line();                           // remove old notices

    usr_cmd = NO_COMMAND; // no command found yet

    if (prompt_cmd == CMD_QUIT) // non hot key entries
      usr_cmd = main_menu->interpret_std_key((int) main_prompt->s[0]);
      
    // catch hot keys; but don't want to overwrite normal entries
    // Note: hot keys may include prompt quit keys
    //   (i.e. those that generate CMD_QUIT and cause a call to the above
    //   interpret) thus the if
    if (usr_cmd == NO_COMMAND)
      usr_cmd = main_menu->interpret_hot_key((int) main_prompt->key);
    usr_cmd = filter_cmd(usr_cmd);

    if (usr_cmd != NO_COMMAND)
    {
      handle_event(usr_cmd,      main_menu, question, working, main_prompt);
      cleanup_prompt(main_prompt, *question);
    }      // fi: proccess cmd
  } // input made
} // user input


void update_clock()
// update the time
{
  time_t *real_time;
  char *time_display;      // tempory for holding the time as it will appear

  real_time = new time_t;
  time(real_time);         // get the time from the system
  time_display = new_str(ctime(real_time));     // convert to printable text
  time_display[strlen(time_display)-1] = '\0';  // replace trailing new line with null terminator

  setpcolour(STATUS_COLOUR);                             // set colour attribute
  dcur_move(TIME_X, STATUS_LINE);               // set cursor position
  dwrite(ctime(real_time));                     // display time}

  delete real_time;
}// update clock



void init()
{
      start_log();
      init_danio();
      dcur(CURSOR_INVIS);
}// init


void setup ()
{
  say_status("Power builder.");
  setpcolour(STATUS_COLOUR);

}//setup

void cloze ()
{
  close_danio();
  end_log();
}//cloze


int main()
{
  // Flow control
  int working; // Flag to control main program loop


  // Prompt stuff
  prompt *main_prompt;
  char *question;
//  int prompt_state = msMAIN_MENU; // the current 'focus' of the prompt - this is a one prompt application

  menu_el *main_menu;
  dwindow *menu_win;

  init();
  setup();
  main_menu = new menu_el("Main Menu",
                new menu_el("~Q~uit", 'q', 0, cmdQUIT,
              NULL));
  main_menu->set_win(menu_win = new dwindow(MENU_WIN_X1, MENU_WIN_Y1, MENU_WIN_X2, MENU_WIN_Y2, "Menu"));
  main_menu->show();


  // Title Bar
  dcur_move(1, TITLE_LINE);
  dwrite("MENU: (q)uit                                                                    ");
  //                                     12345678901234567890123456789012345678901234567890123456789012345678901234567890
  //                                                   1              2              3              4              5              6              7              8


  // create the prompt
  main_prompt = new prompt(PROMPT_LEN, NULL, PROMPT_COLOUR, NO_AUTHORITY);
  question = new_str("Enter your selection: ");
  setpcolour(PROMPT_COLOUR);
  dcur_move(1, PROMPT_LINE);
  dwrite(question);
  main_prompt->pmove(xoMAIN_MENU, PROMPT_LINE);        // NB:  x, y order
  main_prompt->redraw();


  do
  {
    update_clock();  // maintain the time display
    user_input(&question, main_prompt, main_menu, working);    // deal with input driven events and all device inputs

    // place hear time driven events or events driven by program state.

  } while (working);

  cloze();
      
  return errNO_ERROR;
} // main

// end - powers.cc



