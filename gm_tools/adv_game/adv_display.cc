/* adv_display.cc - display object for adv_game	   
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

// used to keep track of the screen windows and as a convenient way to 
//   initilise them. Objects which display themselves should keep a pointer to
//   an adv_display object for their own refrence. 


#include <assert.h>

#include "adv_display.h"
#include "dandefs.h"
#include "sat.h"
#include "danio.h"
#include "err_log.h"
			 		 
adv_display::adv_display()	   	 	       
{ 	     	       	   	   	 	       
  description_win = new dwindow(DESCRIPTION_X, DESCRIPTION_Y, 
       	       	       	   DESCRIPTION_X2, DESCRIPTION_Y2, "description");
  setpcolour(GENERAL, description_win);  		    
  dclear(description_win);     	      	  		    
  dwrite(description_win, "description_win\n");	    
  drefresh(description_win);   	       	    	       	    
  		 	       	      	  		    
  event_win = new dwindow(EVENT_X, EVENT_Y, EVENT_X2, EVENT_Y2, "event");
  setpcolour(GENERAL, event_win);	  	    	    
  dclear(event_win);	       	      	    	    	
  dwrite(event_win, "event_win\n");  	  	    	
  drefresh(event_win);	       	 		    
			       	 		    
  detail_win = new dwindow(DETAIL_X, DETAIL_Y, DETAIL_X2, DETAIL_Y2, "detail");
  setpcolour(GENERAL, detail_win);		    	
  dclear(detail_win);	    			    	
  dwrite(detail_win, "detail_win\n");		    	
  drefresh(detail_win);	   				
  			  
  detail_state = detMENU;  				
  combined = FALSE;	   	
}// adv display constructor    	   		       	  
     	     	   	       	   		       	  
void adv_display::combine()    
// combine description and event windows
// PRECONDITION: display is expanded
{ 	   	   	       
  assert(combined == FALSE);   
  if (description_win != NULL) delete description_win;
  description_win = new dwindow(COMBINED_X, COMBINED_Y,
  	     	       	   COMBINED_X2, COMBINED_Y2, "combined");   
  setpcolour(GENERAL, description_win);
  dclear(description_win); 	    	  	      	       
  dwrite(description_win, "combined window\n");    	       
  drefresh(description_win);	    	    	      	       
  if (event_win != NULL) delete event_win; 		       
  event_win = description_win;	    	   		       
      		    	   	    	   		       
  log_event("View combine completed.\n");	   	       	   	      
  combined = TRUE; 	   	    	   	      	       
} // combine	       	   	    	   	      	       
  	   	       	   	    	   	      	       
void adv_display::expand() 	       	   	      	       
// seperate description and event windows 		       
// PRECONDITION: display is combined 	      		       
{  			   	    			       
  assert(combined == TRUE);				       
  if (description_win != NULL) {			       
    delete description_win;				       
    event_win = NULL;					       
  } // fi						       
  	 						       
  description_win = new dwindow(DESCRIPTION_X, DESCRIPTION_Y,  
  	     	       	   DESCRIPTION_X2, DESCRIPTION_Y2, "description");    
  setpcolour(DESCRIPTION_COLOUR, description_win);
  dclear(description_win);	    	   	      	       
  dwrite(description_win, "description window\n");    	       
  drefresh(description_win);   	       	      	      	       
//  if (event_win != NULL) delete event_win; // unneccessary as description_win and event_win are the same, in fact this could cause a seg fault       	 
  event_win = new dwindow(EVENT_X, EVENT_Y, EVENT_X2, EVENT_Y2, "event");
  setpcolour(EVENT_COLOUR, event_win);	      		       
  dclear(event_win);	    	    	      		       
  dwrite(event_win, "event window\n");   	      	       
  drefresh(event_win);	    	   	      	       	       
  log_event("expand");		  	      		       
  combined = FALSE;		  	      		       
} // expand	       		  			       
		       		  			       
// end - display.cc    		  			       
		   		  			       
							       
