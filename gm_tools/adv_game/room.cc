/*  room.cc - rooms for adv_game
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#include <stdlib.h>
#include <string.h>

#include "room.h"  
#include "dandefs.h"
#include "status_line.h"
#include "err_log.h"

room::room()	     	 	   	  
// construct an empty, isolated room	  
{	   	     		    	  
  for (int i = 0; i < MAX_DOORS; i++)	  
    nabour[i] = NULL;	   	    	  
  way_up = NULL;     	   	    	  
  way_down = NULL;
  id = 0;
  has_monster = 0;
  num_occupants = 0;
} // room constructor	   	    	  		  	
     	      	     	   	    	  
int room::save(FILE *out)	    	    
{		     	 	    	    
  int count;	     	 	    	    
  fprintf(out, "%d ", this);	    	    
  fprintf(out, "%d ", has_monster); 	  
  for (count = 0; count < MAX_DOORS; count++)
    fprintf(out, "%d ", nabour[count]);	    
  fprintf(out, "%d ", way_up);	    	    
  fprintf(out, "%d ", way_down);    	    
  fprintf(out, "%d ", num_occupants);	    
  for (count = 0; count < num_occupants; count++)
    fprintf(out, "%d ", occupant_list[count]);
  fprintf(out, "\n");		    	  	
  return SUCCESS;		    	  	
} // save			    	  	
	 			    	  	
int room::load(FILE *in)		    	  	 
// reads in the data from a previous save game		 
// WARNING: this does not link the rooms up, it just stores the data for
//   the linker.					 
{	     	      		    	  		 
  int count; 	      		    	  		 
  char temp[MAX_INPUT];			   		 
  char *sptr;				   		 
	     						 
  log_event("room::load()\n");		       	       	 
  // read in the room data			
  if (fgets(temp, MAX_INPUT, in) == NULL)	
  {						
    log_event("room::load - Input file abnormally truncated.\n");
    say_status("Save file abnormally truncated, load failed.\n");
    return errEOF;				
  } else					
    log_event("room::load - details: %s\n", temp);
    						
    						
  sptr = strtok(temp, " ");  // the room id
  sscanf(sptr, "%d", &id);  	       		 
  log_event("room::load - Room id read off.\n");
  sptr = strtok(NULL, " ");  // has monster  	
  log_event("room::load - has_monster read off: %s.\n", sptr);
  sscanf(sptr, "%d", &has_monster);  	       		 
  log_event("room::load - has_monster loaded: %d\n", has_monster);
  	     				       	
  for (count = 0; count < MAX_DOORS; count++)  	
  {	     	    		     	       	
    sptr = strtok(NULL, " "); // door  	       	
    sscanf(sptr, "%d", &nabour[count]);	       	  
  }	       	       	       		       	
  log_event("room::load - doors loaded\n");    
  	       				       
  sptr = strtok(NULL, " ");  // way up         		   
  sscanf(sptr, "%d", &way_up);	      	       
  sptr = strtok(NULL, " ");  // way down       	      	   
  sscanf(sptr, "%d", &way_down);	       	  
  sptr = strtok(NULL, " ");  // number of occupants  	       	   
  sscanf(sptr, "%d", &num_occupants);	       	   
  log_event("room::load - num_occupants loaded: %d\n", num_occupants);
	    	    			       
  for (count = 0; count < num_occupants; count++)  
  {	    	    			       	   
    sptr = strtok(NULL, " ");  	              // position in character list, 
    sscanf(sptr, "%d", &occupant_list[count]); // to be stored in occupant list
  }	       			  	       
  log_event("room::load - occupant list loaded.\n");
	       			  	   
  log_event("room::load - end\n");
  return SUCCESS;	      	  	    
}// load       		      	  	    
  	       		      	  
int room::is_closed()	      	  
// returns TRUE if all exits are closed, else FALSE 
{      	       	    	   
  for (int i = 0; i < MAX_DOORS; i++)
  {    	       	    	   
    if (nabour[i] != NULL) 
    {	       		   
//      log_event("Room open\n");
      return FALSE;  	       
    }  	      	    	       
  }// for: all doors	       
       	      		   
//  log_event("Room closed\n");
  return TRUE;	       	       
} // is closed	    	   
       			   
       			   
       	
// end - room.cc
