/* room.h - rooms for adv_game
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
  
#ifndef ROOM_H		  
#define ROOM_H		  
  
#define DEBUG_DUNGEON
  
#include "dandefs.h"
#ifdef DEBUG_DUNGEON	  
#define MAX_DOORS 4	  // ways rooms are connected
#define DUNGEON_LEVELS 3 // levels	    
#define DUNGEON_SIZE_Y 10 // rooms north-south
#define DUNGEON_SIZE_X 10 // rooms east-west
#define WAYS_UP	  1    	  // one way passages up from below per level
#define WAYS_DOWN 5        // one way passages down per level
#define WAYS_UP_AND_DOWN 1   // two way passages down (and back) per level
#define ONE_WAY_PASSAGES 5  // one way latteral passages per level
#define TWO_WAY_PASSAGES 5 // extra latteral passages per level
#else       	       	    	 
#define MAX_DOORS 4	  // ways rooms are connected
#define DUNGEON_LEVELS 20 // levels	    
#define DUNGEON_SIZE_Y 20 // rooms north-south
#define DUNGEON_SIZE_X 20 // rooms east-west
#define WAYS_UP	  1    	  // one way passages up from below per level
#define WAYS_DOWN 25        // one way passages down per level
#define WAYS_UP_AND_DOWN 4   // two way passages down (and back) per level
#define ONE_WAY_PASSAGES 50  // one way latteral passages per level
#define TWO_WAY_PASSAGES 50 // extra latteral passages per level
#endif			  
  			  
#define NORTH 0		  
#define SOUTH 1
#define EAST 2
#define WEST 3
#define BEARING_ERR 4
       
#define MAX_BUDS 400	  
#define MAZE_MAX_PATH 5 	  // changed from MAX_PATH to avoid conflict with stdlib under dev-cpp
#define in_bounds(x,y) (((x) < DUNGEON_SIZE_X) && ((x) >= 0) && ((y) >= 0) && ((y) < DUNGEON_SIZE_Y))
       	     
#define MAX_OCCUPANTS 10 // maximum characters in a room

struct point
{      
  int x;
  int y;
};     
       
       
class room	  	 		  
{      		  	 		  
public:		  			  
  room(); // constructor 
  int is_closed();  // returns TRUE if all exits are closed, else FALSE
  int save(FILE *out);	 
  int load(FILE *in);

  int id; // used in loading the game to link characters to rooms.
  int has_monster;			     
  room* nabour[MAX_DOORS];
  room* way_up;	  	 		  
  room* way_down; 	 
  int num_occupants;  // number of characters in the room
  int occupant_list[MAX_OCCUPANTS];  // list of indexes for occupants
};// room object      	 
		      	 
#endif	
// end - room.h
