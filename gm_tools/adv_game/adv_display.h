/*  adv_display.h - display object for adv_game	 
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
  
// used to keep track of the screen windows and as a convenient way to 
//   initilise them. Objects which display themselves should keep a pointer to
//   an adv_display object for their own refrence. 
	   
#ifndef DISPLAY_H				      
#define DISPLAY_H

#include "danio.h"
#include "status_line.h"

// display lines
#define MENU_LINE 1
const int MENU_COLOUR = pcolLIGHTGRAY; // was 103 for cursio
	    
#define ADV_PROMPT_LEN 30   
	    	      	    
// display windows    	    
#define DESCRIPTION_LINES 10
#define DESCRIPTION_COLS  50
#define DESCRIPTION_Y      2
#define DESCRIPTION_X      1
#define DESCRIPTION_Y2 	  11
#define DESCRIPTION_X2 	  50 
#define DESCRIPTION_COLOUR 2			       
	    	       	    			       
#define EVENT_LINES 12 	    			       
#define EVENT_COLS  50 	    			       
#define EVENT_Y     12 	    			       
#define EVENT_X      1 	    			       
#define EVENT_Y2    23  	    
#define EVENT_X2    50   	    
#define EVENT_COLOUR 1 	    
#define EVENT_LINE  20	    
		      	    
#define COMBINED_LINES 22   
#define COMBINED_COLS  50   
#define COMBINED_Y      2   
#define COMBINED_X      1      	
#define COMBINED_Y2   23
#define COMBINED_X2   50 	
	    	       		
#define DETAIL_LINES 22	 	
#define DETAIL_COLS  29	 	
#define DETAIL_Y      2	 	
#define DETAIL_X     52		
#define DETAIL_X2    80  
#define DETAIL_Y2    23  
#define DETAIL_COLOUR 71	    
	     	       	
// details options    				       
#define detMENU 1   // show the main menu   	       	       	  	       
#define detCHAR 2   // show the character 
#define detCOMBAT 3  // show the guts of combat 
#define detTRADE 4  // show trading screen

// colours	
#define GENERAL	 7 // grey
#define IMPORTANT 71 // bright white
#define ACTION	 2 // movement      
#define CAUTION  3 // amber
#define WARNING  65 // taking damage      	 

class adv_display     	    		       	       
{
private: 	
  int combined; 
   	       	
public:        	      	    		       	       
  adv_display(); // initilises display windows from the above constants
  void combine(); // combine description and event windows
  void expand();// seperate description and event windows
  int is_combined() { return combined; }
  dwindow *description_win;	      	       	       
  dwindow *event_win;     	  	       	       
  dwindow *detail_win;			       
  int detail_state;			       
}; // game display    	  	  	       	       
  	       	      			       
  	       	      
#endif	       	      
// end - adv_display  
  	    	      
		      
