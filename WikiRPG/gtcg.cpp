/* gtcg.cpp - Generic Text Collection Generator WikiRPG Generic Pages http://www.wikirpg.com/en/index.php/Main_Page
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
 
 /* Instructions for use
  * This program is only useful for generating the Generic Text Collections used in the WikiRPG pages at http://www.wikirpg.com/en/index.php/Main_Page
  * 1. go to special pages
  * 2. select all
  * 3. cut and paste into the page http://www.wikirpg.com/en/index.php?title=Generic_Text:Table_of_Contents&action=edit
  * 4. format into a table of contents using the format:
  *    * [[Generic Text:Collection:Collection Name 1]]
  *    ** [[Generic Text:Something Generic 1]]
  *    ** [[Generic Text:Something Generic 2]]
  *    ** [[Generic Text:Something Generic 3]]
  *    * [[Generic Text:Collection:Collection Name 2]]
  *    ** [[Generic Text:Something Generic 4]]
  *    ** [[Generic Text:Something Generic 5]]
  *    ** [[Generic Text:Something Generic 6]]'
  *    etc...
  * 5. copy the raw text from the edit widow to a text file called "Generic TOC" in the same directory as this program
  * 6. execute the program
  * 7. copy the output in the files generated into the appropriate pages.
  *
  */

#include <iostream.h> // stream operations
#include <fstream.h>  // file io

const char file_name[] = "Generic TOC";
const char collection_all[] = "generic-collection-all";
const char collectionIntro[] = "{{Generic Text Collection}} [[Template:Generic Text Collection|--ed]]";
char c; // tempory character buffer
char buff[256]; // tempory character buffer
int pos=0; // position in buffer
int level=0; // table depth (number of asterixs '*' at beggining of line)
int squareBraket = 0;
char CollectionName[256]; // tempory character buffer
int CollectionNamePos = 0; // position in collection name buffer
int i; // loop counter
int OpenCollection = 0;
int TITLE_OFFSET = 13; // offset into collection name for title
const char up[] = "[[Generic Text]]";
char prev[256]; 
char current[256];
char next[256];

ofstream collection; // collection file handle

int main()
{
  strcpy(prev, up); strcpy(current, up); strcpy(next, up); // setup the nav bar titles to defult up
  // open the input file (http://www.wikirpg.com/en/index.php?title=Generic_Text:Table_of_Contents&action=edit)
  ifstream in(file_name, ios::in);
  if (!in)
  {
    cout << "Fatal error - working file " << file_name << " cannot be opened for input."  << endl;
    exit(1);
  }//fi 
  
  cout << "Creating file \"" << collection_all << '\"' << endl;
  ofstream all(collection_all, ios::out);
  if (!all)
  {
    cout << "Cannot open " << collection_all << " for output." << endl;
    exit(1);
  }//fi
  
  // put the intro in the file
  all << collectionIntro << endl << endl;
  
  
  while(in)    // display the file
  {
    in.get(c); //   read one character from the file
 //   cout << '<' << c << '>' << endl;
    buff[pos] = c; // store in buffer
    pos++;
    //cout << c;     //   write one character to screen
    if (c == ']') // close square braket
    {
      squareBraket--;
      CollectionName[CollectionNamePos] = 0;
    }//fi
    if (squareBraket == 2) // inside 2 square brakets
    {
      CollectionName[CollectionNamePos] = c;
      CollectionNamePos++;
    }
    if (c == '*') // level mark
    {
      level++;
    }
    if (c == '[') // open square braket
    {
      squareBraket++;
    }//fi
    
    
    if (c == '\n') // end of line
    {
      if (level == 1) 
      {
	  strcpy(prev, current);
	  strcpy(current, next);
	  strcpy(next, CollectionName);
        cout << "End line at level 1." << endl << "  Collection: \"" << CollectionName << '\"' << endl << "  buff: \"" << buff << '\"' << endl;
        for (i = 0; CollectionName[i] ; i++) 
	  if (CollectionName[i] == ':' || CollectionName[i] == ' ') CollectionName[i] = '-'; //remove colons and replace with dash
        cout << "Creating file \"" << CollectionName << '\"' << endl;
	
	if (OpenCollection) // there is currently a collection open
	{
	  cout << "Closing collection \"" << CollectionName << '\"' << endl;
	  // put the end into the file (nav bar)
	  collection << "{{ FrameBar|Main=[[Main Page|WikiRPG Main Page]] | Prev=[[" << prev << "]] | Up=[[Generic Text]] | Next=[[" << next << "]] }}";
	  // close the file
	  collection.close();
	}
	
	cout << "Opening collection \"" << CollectionName << '\"' << endl;
	collection.open(CollectionName, ios::out);
	if (!collection)
	{
	cout << "Cannot open " << CollectionName << " for output." << endl;
	exit(1);
	} else OpenCollection = 1;
	// put the intro in the file
	collection << collectionIntro << endl << endl;

      }//fi
      else if (level == 2) 
      {
        cout << "End line at level 2." << endl << "  Collection: \"" << CollectionName << '\"' << endl << "  buff: \"" << buff << '\"' << endl;
      	cout << "adding entry \"" << CollectionName << '\"' << endl;
      	all << "==" << (CollectionName+TITLE_OFFSET) << "==" << endl << endl << "{{:" << CollectionName << "}} [[" << CollectionName  << "|--ed]]" << endl << endl;
      	collection << "==" << (CollectionName+TITLE_OFFSET) << "==" << endl << endl << "{{:" << CollectionName << "}} [[" << CollectionName  << "|--ed]]" << endl << endl;
      } else { 
        cout << "End line at level ?." << endl << "  Collection: \"" << CollectionName << '\"' << endl << "  buff: \"" << buff << '\"' << endl;
      }//fi
      
      buff[pos] = 0;
      squareBraket = 0; // should be anyway unless the file is formatted incorrectly
      CollectionName[0] = 0;
      CollectionNamePos = 0;
      pos = 0;
      level = 0;
    }//fi: end of line
    
  }//while   
  cout << endl;    
  // put the end into the file (nav bar)
  all << "{{ FrameBar|Main=[[Main Page|WikiRPG Main Page]] | Prev=[[" << up << "]] | Up=[[Generic Text]] | Next=[[" << up << "]] }}";
  // close files
  cout << "Closing collection \"" << CollectionName << '\"' << endl;
  // put the end into the file (nav bar)
  collection << "{{ FrameBar|Main=[[Main Page|WikiRPG Main Page]] | Prev=[[" << prev << "]] | Up=[[Generic Text]] | Next=[[" << next << "]] }}";
  // close the file
  in.close(); all.close(); collection.close();
}//main


