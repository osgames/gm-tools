/*  menu.h - menuing system
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

// The menuing system is intended to make adding menues to applications easy
// by providing functions which build up a menu at run time. The application
// programmer should only need to ammend his function calls, his command 
// definitions and his event handler to make changes to the menu.

// application command values must be greater than 100
// see below for special menu commands

#ifndef MENU_H 			 	 
#define MENU_H 		       	 	 

#include "danio.h"

// ************  M E N U   C O M M A N D S
		
// not any key code (0 is no key returned, -1 is no key pressed)
#define menuNO_KEY -2
#ifndef NO_COMMAND
#define NO_COMMAND 0
#endif	 	
#define UP_MENU_TREE 1  	 	      			     
	 	
class menu_el  	      	       	 	     
{	       	      	       	 	     
public:	       	      	       	 	     	  
  menu_el(const char *new_title, int new_key, int new_hot_key, int new_cmd,
       	  menu_el *new_sub, menu_el *new_next);
  menu_el(const char *new_title, int new_key, int new_hot_key, int new_cmd,
	  menu_el *new_next);		       		      		   
  menu_el(const char *new_title, int new_key, int new_cmd, menu_el *new_next);
  menu_el(const char *new_title, menu_el *new_next);	       
    // a variety of constructors are available to handle different types of 
    // menu ellements providing different amount of information
  void show();                            
    // display the menu                   
  int interpret(int d);	       	  	      
    // find a command that matches the input d
  int interpret_std_key(int d);           
    // search the menu for a match to standard input only
  int interpret_hot_key(int d);           
    // search the menu for a hot key match			 
  void set_win(dwindow *new_win);         
    // force the menu to appear in a window
protected:	       			                  
  void assign_all(const char *new_title, int new_key, int new_hot_key, 
          	  int new_cmd, menu_el *new_sub, menu_el *new_next);
    // do all the assignments for the constructors
  char *title; 	       	       		      
  menu_el *next;       	       		     
  menu_el *sub;	       			     
  int key;     // key code to select from menu
  int hot_key; // hot key code to select from anywhere in menu hirachy
  int cmd;     // cmd is return(ed) by interpret() when menu_el is 'selected'
  static menu_el *top; 			   
  static menu_el *pos;			   
  static dwindow *menu_win;
}; // menu element     	       		     
	       	       	       		     
       	      	     	       	  	     
#endif 	      	     	       	  	     
       	      		       	  	   
// end - menu.h	   		  	   
				  	   
					   
