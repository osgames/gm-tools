#prompt.mk - includeable makefile for prompt project
#Daniel Vale MAY 2002

ifndef PROMPT_MK
PROMPT_MK = 1

include $(INCLUDE_DIR)/map.mk

$(PROMPT_DIR)/prompt.o : $(PROMPT_DIR)/prompt.cc $(PROMPT_DIR)/prompt.h \
                         $(DANIO_H) $(DANIO_DIR)/keys.h
	g++ -c $(DIR_LIST) -o $(PROMPT_DIR)/prompt.o $(PROMPT_DIR)/prompt.cc

endif
