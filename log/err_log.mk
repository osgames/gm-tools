# log.mk - includeable make file for the error log

ifndef LOG_MK
LOG_MK = 1

include $(INCLUDE_DIR)/map.mk

$(LOG_DIR)/err_log.o : $(LOG_DIR)/err_log.h $(LOG_DIR)/err_log.cc $(INCLUDE_DIR)/dandefs.h 
	g++ -o $(LOG_DIR)/err_log.o -c $(DIR_LIST) $(LOG_DIR)/err_log.cc 

include $(INCLUDE_DIR)/include.mk

endif

