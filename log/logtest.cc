/* logtest.cc - testing of error loging
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see 
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

#include <iostream>
#include <assert.h>

#include "dandefs.h"
#include "err_log.h"

using namespace std; 
  
int main()
{
  if (start_log(logALL))
  {
    cout << "logtest: Fatal error - failed to start log. Possible IO error." << endl;
    exit(errINIT_FAILED);
  }//fi
  
  // all test
  assert(log_event(logERROR, "Error level\n") == errNO_ERROR);
  assert(log_event(logWARNING, "Warning level\n") == errNO_ERROR);
  assert(log_event(logCONTROL, "Control level\n") == errNO_ERROR);
  assert(log_event(logDEFAULT, "Default level\n") == errNO_ERROR);
  assert(log_event(logALL, "ALL level\n") == errNO_ERROR);
  
  //default test
  log_event("\nSetting logging to Default.\n");
  set_log_level(logDEFAULT);
  assert(log_event(logERROR, "Error level\n") == errNO_ERROR);
  assert(log_event(logWARNING, "Warning level\n") == errNO_ERROR);
  assert(log_event(logCONTROL, "Control level\n") == errNO_ERROR);
  assert(log_event(logDEFAULT, "Default level\n") == errNO_ERROR);
  assert(log_event(logALL, "ALL level\n") == errREDUNDANT);
  
  //control test
  log_event("\nSetting logging to Control.\n");
  set_log_level(logCONTROL);
  assert(log_event(logERROR, "Error level\n") == errNO_ERROR);
  assert(log_event(logWARNING, "Warning level\n") == errNO_ERROR);
  assert(log_event(logCONTROL, "Control level\n") == errNO_ERROR);
  assert(log_event(logDEFAULT, "Default level\n") == errREDUNDANT);
  assert(log_event(logALL, "ALL level\n") == errREDUNDANT);

  //Warning test
  log_event("\nSetting logging to WARNING.\n");
  set_log_level(logWARNING);
  assert(log_event(logERROR, "Error level\n") == errNO_ERROR);
  assert(log_event(logWARNING, "Warning level\n") == errNO_ERROR);
  assert(log_event(logCONTROL, "Control level\n") == errREDUNDANT);
  assert(log_event(logDEFAULT, "Default level\n") == errREDUNDANT);
  assert(log_event(logALL, "ALL level\n") == errREDUNDANT);
  
  //Error test
  log_event("\nSetting logging to Error.\n");
  set_log_level(logERROR);
  assert(log_event(logERROR, "Error level\n") == errNO_ERROR);
  assert(log_event(logWARNING, "Warning level\n") == errREDUNDANT);
  assert(log_event(logCONTROL, "Control level\n") == errREDUNDANT);
  assert(log_event(logDEFAULT, "Default level\n") == errREDUNDANT);
  assert(log_event(logALL, "ALL level\n") == errREDUNDANT);
  
  //Warning test
  log_event("\nSetting logging to None.\n");
  set_log_level(logNONE);
  assert(log_event(logERROR, "Error level\n") == errREDUNDANT);
  assert(log_event(logWARNING, "Warning level\n") == errREDUNDANT);
  assert(log_event(logCONTROL, "Control level\n") == errREDUNDANT);
  assert(log_event(logDEFAULT, "Default level\n") == errREDUNDANT);
  assert(log_event(logALL, "ALL level\n") == errREDUNDANT);
    
  end_log();
  
  cout << "Test complete. View err.log for the results." << endl;
  return 1;
}//main 