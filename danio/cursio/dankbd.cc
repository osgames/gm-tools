/*  dankbd.cc - keyboard wrapper code for danio project
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
     
// curses.h version - implements dankbd with curses.h
// This implementation provides curses keyboard input
   	      	 
// it may not be possible to use ncurses keyboard input without the associated
// screen output, especially initscr() ???

#include <curses.h>
#include <stdarg.h> 

#include "keys.h"  // key mnemononics
#include "dankbd.h"

int init_dankbd() 
// initialise ncurses
{  	       	     
//  initscr();  // not sure if this will work?
//  start_color();     	   
  nonl();// give ncurses control of carrage returns and allow detection of return key
  cbreak();// disable line buffering
  noecho();    	       	   	    
  keypad(stdscr, TRUE);// ncurses interprets function keys as single value input
  return 0;	 
} // init danio	 
	   	 
int dgetch() 	 
{	      	 
  return getch();
}//dgetch  	 
       	     	 
void close_dankbd()
{ 	   
//  endwin();
}	   
	   
void dgetch_wait(int ms)
  // set the wait period for dgetch() in milli seconds
  // -1 for unlimited wait, 0 for no wait
{			
  if (ms == 0)
    nodelay(stdscr, TRUE);
  else if (ms == -1)
    nodelay(stdscr, FALSE);
  else halfdelay(ms);
} // dgetch_wait
