/* dwindow.h - windowing encapsulation for danio project
 *
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

// NOTE: application programs should not access dwindow members directly
//   but do so through other danio functions
// ncurses version

#include <curses.h>

#include "dwindow.h"
#include "danio.h"
#include "sat.h"
#include "err_log.h"
#include "dandefs.h"

dwindow::dwindow(int new_x1, int new_y1, int new_x2, int new_y2,
             char *new_name)
// window constructor
{
  x1 = new_x1; y1 = new_y1; x2 = new_x2; y2 = new_y2;
  win_ptr = newwin(y2-y1+1, x2-x1+1, y1-1, x1-1);
  wsetscrreg((WINDOW *) win_ptr, 0, new_y2 - new_y1 + 1);
  scrollok((WINDOW *) win_ptr, TRUE);
  if (new_name != NULL)
    name = new_str(new_name);
}// dwindow constructor


void dwindow::setscroll(int scrl)
// turn scolling on or off
{
  dset_scroll(scrl, this);
} // set scr



void dwindow::dwrite(char *s, ...)
// formatted output to a window
{
  va_list ap; // variable argument list

  log_event("dwindow::dwrite() - start\n");

  va_start(ap, s); // setup variable argument list

  vwprintw((WINDOW*) win_ptr, s, ap);  // print to screen using cursio's vwprintw()

  va_end(ap); // finish with variable argument list

  log_event("dwindow::dwrite() - end\n");

}// dwrite

void dwindow::dvwrite(char *s, va_list ap)
// formatted output to a window using a processed argument list
{
  vwprintw((WINDOW*) win_ptr, s, ap);  // print to screen using cursio's vwprintw()
}//dwrite

void dwindow::dcur_move(int new_x, int new_y)
// move the cursor
{
  wmove((WINDOW *) win_ptr, new_y - 1, new_x - 1);
}// dcur_move

void dwindow::get_dcur_xy(int& x, int& y)
// find the position of the cursor
{
  getyx((WINDOW *) win_ptr, y, x); // get positions from ncurses
  x++; y++; // make correction to danio co-ordinates
}// get_dcu_xy


int dwindow::get_dcur_x()
// find the column
{
  int x, y;

  getyx((WINDOW *) win_ptr, y, x);

  return x;
}// get_dcu_x

int dwindow::get_dcur_y()
// find the row
{
  int x, y;

  getyx((WINDOW *) win_ptr, y, x);

  return y;
}// get_dcu_y

void dwindow::dclear()
// clear the window
{
  log_event("dclear (win: %s) \n", name); // log the event
  wclear((WINDOW *) win_ptr);          // clear the window
}// dclear

void dwindow::drefresh()
// redraw the screen and flush any buffered screen printing
{
  log_event("drefresh - win:%s\n", name);
  wrefresh((WINDOW *) win_ptr);
}// drefresh


/*
 *
 *                       COLOUR
 *
 */

int dwindow::setpcolour(int new_colour)
  // change the printing colour
{
//  log_event("dwindow::setpcolour() - start\n");

  if (new_colour >= COLOR_PAIRS) // bold ?
  {
    wattron((WINDOW*) win_ptr, A_BOLD);   // bold on
    wcolor_set((WINDOW*) win_ptr, new_colour - COLOR_PAIRS, NULL);
  }
  else
  {
    wattroff((WINDOW*) win_ptr, A_BOLD);  // bold off
    wcolor_set((WINDOW*) win_ptr, new_colour, NULL);
  }
//  log_event("dwindow::setpcolour() - end\n");

  return errNO_ERROR;
}// setpcolour



// end - dwindow.cc ncurses version

