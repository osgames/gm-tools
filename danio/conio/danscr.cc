/*  danscr.cc - screen code for danio project
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
      
// curses.h version - implements danscr with curses.h
// This implementation provides advanced screen text output
// NOTE: ncurses uses top left co-ordinate of (0,0) whilst danio uses (1,1)
//   not all conversions have been properly made yet FIX ME
   	      	 			       	    	   
#include <conio.h>					   
#include <stdio.h>
#include <stdarg.h>					   

#include "danscr.h"
#include "dwindow.h"
#include "dandefs.h"  
#include "err_log.h"
#include "sat.h"
#include "pallet.h"
		       
// screen dimentions, assigned values at runtime by init_danscr()
int dcur_maxx, dcur_maxy;
void set_pallet(); // setup the colour pallet
		       			     
int init_danscr()      			     
// initialise vidieo output		     
{  	       	       	   		     
  dcur_maxy = 50; dcur_maxx = 80; // FIX ME
  // setup colour pallet
  set_pallet();		       		

  return 0;	     	       	  	     
} // init danio	     	       		     

void dset_scroll(int new_scrl, dwindow *win /* = NULL */)
// turn window (or screen) scrolling on or off 	   
// defaults to full screen
// DEPRECIATED: Use of dset_scroll(scrl, win) is depreciated, use win->setscroll(scrl) instead
//              dset_scroll(scrl) will contune to be supported for the default window (stdscr)
//              Support for dset_scroll(scrl, win) is for backward compatability only
// stub - FIX ME
{   	       	
  if (win) 
    win->setscroll(new_scrl);
  else
    log_event("dset_scroll() - no scroll controll implemented for conio.\n");      
     
}// dset scroll       	       		    

	       	      	       		    
void dwrite(char *s, ...)    		    
{ 	       	     	       		    
  va_list ap;        	       		    
      	      	       	     
  va_start(ap, s); 
  vprintf(s, ap);   	       	     
  va_end(ap);              	  
   	      	     
} // dwrite   	     	 


void dwrite(dwindow *win, char *s, ...)
// write to a window	 
// DEPRECIATED: us win->dwrite instead. Support for dwrite(win, s) is for backward compatability only
{
  va_list ap;        	     
      	      	       	     
  va_start(ap, s); 	 

  if (win != NULL)
    win->dvwrite(s, ap);
  else	   	     	 
    vprintf(s, ap);

  va_end(ap);              	  
} // dwrite   	     	 

void dvwrite(char *s, va_list ap)
// formated output, using a va_list previously extracted
{ 
  vprintf(s, ap);
}// dvwrite	    

void dvwrite(dwindow *win, char *s, va_list ap)
// formated output, using a va_list previously extracted
// DEPRECIATED: use win->dvwrite(s, ap), support for dvwrite(win, s, ap) is for backward compatability only
{ 		    
  if (win != NULL)   	 
    win->dvwrite(s, ap);
  else	   	     	 
    vprintf(s, ap);
}// dvwrite	    

void close_danscr() 
{	   	    
  // nothing to do here
}    	   	    

void dcur_move(int x, int y, dwindow *win)
// DEPRECIATE USAGE: use of dcur_move(x, y, win) is depreciated, use win->dcur_move(x, y) instead
//                   dcur_move(x, y, win) is for backward compatability only
//                   dcur_move(x, y) will continue to be supported for the standard window (stdscr)
{    	       				 
  if (win != NULL)			 
    win->dcur_move(x, y);
  else	      				 
    gotoxy(x-1, y-1);			 
} // dcur_move

int get_dcur_x(dwindow *win)
// DEPRECIATED USAGE: use of get_dcur_x(win) is depreciated, use win->get_dcur_x() instead
//                    use of get_dcur_x() will continue to be supported for the standard window (stdscr)
{   	   	      	   

  if (win == NULL)	     
    return wherex() + 1;     
  else	  		     
    return win->get_dcur_x();
} // get_decu_x
	  	   
void dcur(int new_mode)
// change the cursor appearance
{ 	  	   

  _setcursortype(new_mode);
}	  	    
	  	    
int get_dcur_y(dwindow *win)
// DEPRECIATED USAGE: use of get_dcur_y(win) is depreciated, use win->get_dcur_y() instead
//                    use of get_dcur_y() will continue to be supported for the standard window (stdscr)
{	     	      
  if (win != NULL)
    return win->get_dcur_y();
  else  
    return wherey() + 1;     
} // get_dcur_y
      
void dclear(int colour /* = BACKGROUND */)
  // clear the screen, optionally with a colour
{ 
  log_event("dclear()\n");
  clrscr(); // colour is ignored as ncurses clear() doesn't have this option
  if (colour != BACKGROUND)
    log_event("dclear() - conio implementaion doesn't clear to other than background colour yet.\n");
} // dclear	      

void dclear(dwindow *win)
// DEPRECIATED: use win->dclear() instead. dclear(win) is supported for backward compatability only
{      	      
  if (win != NULL)
    win->dclear();
  else
    dclear();

} // dclear	       			    

void dcleareol()       			    
// clear from the cursor to the end of the line
{		       			    
  clreol();	       			    
}		       			    
void drefresh(dwindow *win/* = NULL*/)     	    
// DEPRECIATED USAGE: use of drefresh(win) is depreciated, use win->drefresh() instead
//                    support for drefresh(win) is for backwards compatability only
{	      	       	
  // has no effect with conio
  if (win != NULL)     	     		    
  {	
    win->drefresh();		     		    
  } else { 		
    log_event("drefresh - full screen\n");	      
    log_event("drefresh() - has no effect with conio.\n");      
  }						      
  						      
} // drefresh	       				      




//
//                          GRAPHICS STUFF
//
// Note that all of these functions will produce no display output in
// the conio implementation and will log a message with log_event()

void dline(int x1, int y1, int x2, int y2, int colour /* = WHITE */)
  // draw a line from (x1, y1) to (x2, y2)
{
  log_event("dline() - conio: no graphics available to draw line from (%d, %d) to (%d, %d) using colour: %d\n", x1, y1, x2, y2, colour);
} // dline


   	      	       
// 	      	       
//                          COLOUR STUFF		    
//                          				    
   	      
   	      
void set_pallet()      	    
{  	      	       
  // no need to setup colours for conio
  log_event("set_pallet() - no need to setup colours for conio.\n");
} // set pallet     	       	       
   	     	       
int wsetpcolour(dwindow *win, short new_colour)
// Pick a colour from the pallet, for a window.
// Colours from COLOUR_PAIRS to 2*COLOUR_PAIRS-1 are bold
// DEPRECIATED: use win->setpcolour(new_colour) instead
//              support for wsetpcolour is for backwards compatability only
{  		       	 		     
  win->setpcolour(new_colour);
     		       			     
  return errNO_ERROR;				     
} // set pallet colour			     

int setpcolour(short new_colour, dwindow *win)
// Pick a colour from the pallet.	     
// Colours from COLOUR_PAIRS to 2*COLOUR_PAIRS-1 are bold
// DEPRECIATED USAGE: use of setpcolour(new_colour, win) is depreciated, use win->setpcolour(new_colour) instead
//                    support for setpcolour(new_colour, win) is for backward compatability only
//                    will continue to support setpcolour(new_colour) for the standard window (stdscr)
{ 		     			     
//  WINDOW *nwin; 	 // from cursio 

  log_event("setpcolour() - start\n");
  if (win != NULL) 
      win->setpcolour(new_colour);
  //  nwin = (WINDOW*) win->get_win_ptr();  // from cursio
  else
  {
    textbackground(new_colour / 16); // just a guess???
    textcolor(new_colour % 16);
  }// not a dwindow
		       	
  return errNO_ERROR;	
}// set pallet colour
	   	
