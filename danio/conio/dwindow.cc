/*  dwindow.cc - windowing encapsulation for danio project
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

// NOTE: application programs should not access dwindow members directly
//   but do so through other danio functions   	       	   
// conio implementation: 
// note: no windowing is available std in conio - all window handling needs to
//       be catered for here, from scratch - not done yet FIX ME (or use something else?)
		       
#include <conio.h>
#include <stdio.h>

#include "dwindow.h"
#include "dandefs.h"
#include "err_log.h"
#include "sat.h"

dwindow::dwindow(int new_x1, int new_y1, int new_x2, int new_y2,
		 char *new_name)
// window constructor	   		       
// stub - FIX ME
{	      		 
  log_event("dwindow::constructor() - dwindow support for conio is incomplete.\n");  		       
  x1 = new_x1; y1 = new_y1; x2 = new_x2; y2 = new_y2;

//  win_ptr = new char[y2-y1][x2-x1]; // newwin(y2-y1+1, x2-x1+1, y1-1, x1-1);
  // wsetscrreg((WINDOW *) win_ptr, 0, new_y2 - new_y1 + 1);
  // scrollok((WINDOW *) win_ptr, TRUE); 
  scrl = true;
  if (new_name != NULL)
    name = new_str(new_name);
}// dwindow constructor
   	     	      	   
   	     	      	   
void dwindow::setscroll(int new_scrl)
// turn scolling on or off
{ 			   
  scrl = new_scrl; //dset_scroll(scrl, this); 
} // set scr



void dwindow::dwrite(char *s, ...)
// formatted output to a window
// stub - FIX ME
{
  va_list ap; // variable argument list

  log_event("dwindow::dwrite() - start\n");
  log_event("dwindow::dwrite() - dwindow support for conio is incomplete.\n");              

  va_start(ap, s); // setup variable argument list

  // vwprintw((WINDOW*) win_ptr, s, ap);  // print to screen using cursio's vwprintw()
  vprintf(s, ap); // - just print to the screen - FIX ME

  va_end(ap); // finish with variable argument list

  log_event("dwindow::dwrite() - end\n");

}// dwrite

void dwindow::dvwrite(char *s, va_list ap)
// formatted output to a window using a processed argument list
// stub - FIX ME
{
  log_event("dwindow::dvwrite(char* va_list) - dwindow support for conio is incomplete.\n");              
  vprintf(s, ap);
//  vwprintw((WINDOW*) win_ptr, s, ap);  // print to screen using cursio's vwprintw()
}//dwrite

void dwindow::dcur_move(int new_x, int new_y)
// move the cursor
// stub - FIX ME
{
  log_event("dwindow::dcur_move() - dwindow support for conio is incomplete.\n");              
  gotoxy(new_x-1, new_y-1);
//  wmove((WINDOW *) win_ptr, new_y - 1, new_x - 1);	 
}// dcur_move

void dwindow::get_dcur_xy(int& x, int& y)
// find the position of the cursor
// stub - FIX ME
{
  log_event("dwindow::get_dcur_xy() - dwindow support for conio is incomplete.\n");              
  x = wherex(); y = wherey();
//  getyx((WINDOW *) win_ptr, y, x); // get positions from ncurses
  x++; y++; // make correction to danio co-ordinates
}// get_dcu_xy


int dwindow::get_dcur_x()
// find the column
// stub - FIX ME
{
//  int x, y;
  log_event("dwindow::get_dcur_x() - dwindow support for conio is incomplete.\n");              

//  getyx((WINDOW *) win_ptr, y, x);

  return wherex() +1;
}// get_dcu_x

int dwindow::get_dcur_y()
// find the row
// stub - FIX ME
{
//  int x, y;
  log_event("dwindow::get_dcur_y() - dwindow support for conio is incomplete.\n");              

//  getyx((WINDOW *) win_ptr, y, x);

  return wherey();
}// get_dcu_y

void dwindow::dclear()
// clear the window
// stub - FIX ME
{

  log_event("dwindow::dclear() - dwindow support for conio is incomplete.\n");              
  log_event("dclear (win: %s) \n", name); // log the event
  clrscr();
//  wclear((WINDOW *) win_ptr);	    // clear the window
}// dclear
		 
void dwindow::drefresh()
// redraw the screen and flush any buffered screen printing
// stub - FIX ME
{
  log_event("dwindow::drefresh() - dwindow support for conio is incomplete.\n");              

  log_event("drefresh - win:%s\n", name);
//  wrefresh((WINDOW *) win_ptr); 
}// drefresh


/*
 *
 *                       COLOUR
 *
 */

int dwindow::setpcolour(int new_colour)
  // change the printing colour
// stub - FIX ME
{
  log_event("dwindow::setpcolour() - start\n");
  log_event("dwindow::setpcolour() - dwindow support for conio is incomplete.\n");              
/*
  if (new_colour >= COLOR_PAIRS) // bold ?	     
  {	 	       	     		     
    wattron((WINDOW*) win_ptr, A_BOLD);   // bold on 	     	     
    wcolor_set((WINDOW*) win_ptr, new_colour - COLOR_PAIRS, NULL);
  }		       			     
  else		       			     
  {		       			     
    wattroff((WINDOW*) win_ptr, A_BOLD);  // bold off     
    wcolor_set((WINDOW*) win_ptr, new_colour, NULL);	     
  }  		       			     
  log_event("dwindow::setpcolour() - end\n");
*/
  return errNO_ERROR;
}// setpcolour



// end - dwindow.cc ncurses version
