/*  keys.h - list of mnemonic key definitions
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
// NOTE the key definitions must change with varios io interfaces.

// NOTE : not all key combinations have been defined, and key parssing is not
// complete. For any key stroke or combination intended to be used in a program
// first use dankeys to check how it is caught, then check it is covered by a 
// #define

#ifndef KEYS_H
#define KEYS_H

// key definitions
#define danioKEY_F1 187 	  
#define danioKEY_F2 188 
#define danioKEY_F3 189 	  
#define danioKEY_F4 190	  
#define danioKEY_F5 191  	  
#define danioKEY_F6 192   	  
#define danioKEY_F7 193   	  
#define danioKEY_F8 194   	  
#define danioKEY_F9 195   	  
#define danioKEY_F10 196  
#define danioKEY_F11 215   
#define danioKEY_F12 216   
		    
#define danioKEY_CTRL_F1  315	  
#define danioKEY_CTRL_F2  316	
#define danioKEY_CTRL_F3  317  	  
#define danioKEY_CTRL_F4  318 	  
#define danioKEY_CTRL_F5  319 	  
#define danioKEY_CTRL_F6  320  	  
#define danioKEY_CTRL_F7  321  	  
#define danioKEY_CTRL_F8  322  	  
#define danioKEY_CTRL_F9  323  	  
#define danioKEY_CTRL_F10 324  
#define danioKEY_CTRL_F11 343   
#define danioKEY_CTRL_F12 344   
	       	      	
#define danioKEY_UP_ARROW 231
#define danioKEY_DOWN_ARROW 236 
#define danioKEY_LEFT_ARROW 233  	 
#define danioKEY_RIGHT_ARROW 234
	   	
#define danioKEY_CTRL_UP_ARROW 359
#define danioKEY_CTRL_DN_ARROW 364 
#define danioKEY_CTRL_LF_ARROW 361  	 
#define danioKEY_CTRL_RT_ARROW 362
		 	     
#define danioKEY_ALT_UP_ARROW 487 
#define danioKEY_ALT_DN_ARROW 364 
#define danioKEY_ALT_LF_ARROW 489   	 
#define danioKEY_ALT_RT_ARROW 490   

#define danioKEY_INS	  238   	
#define danioKEY_HOME  230   
#define danioKEY_PGUP  232   
#define danioKEY_DEL	  239   
#define danioKEY_END	  235   
#define danioKEY_PGDN  237  
		      
#define danioKEY_CTRL_INS  366   	
#define danioKEY_CTRL_HOME 358   
#define danioKEY_CTRL_PGUP 360   
#define danioKEY_CTRL_DEL  367   
#define danioKEY_CTRL_END  363 
#define danioKEY_CTRL_PGDN 365 
		     
#define danioKEY_MINUS       45	
#define danioKEY_PLUS        43	 
#define danioKEY_CTRL_MINUS 330	 
#define danioKEY_CTRL_PLUS  334	  
#define danioKEY_ALT_MINUS  458	
#define danioKEY_ALT_PLUS   462	  
		     
#define danioKEY_BACKSPACE 8	
#define danioKEY_ENTER 13 
#define danioKEY_ESC 27

// CTRL_ALPHA - only use these if strict ascii is off
#define danioKEY_CTRL_Q 272   
#define danioKEY_CTRL_W 273
#define danioKEY_CTRL_E 274
#define danioKEY_CTRL_R 275   
#define danioKEY_CTRL_T 276   
#define danioKEY_CTRL_Y 277   
#define danioKEY_CTRL_U 278   
#define danioKEY_CTRL_I 279   
#define danioKEY_CTRL_O 280  
#define danioKEY_CTRL_P 281   
#define danioKEY_CTRL_A 286   
#define danioKEY_CTRL_S 287   
#define danioKEY_CTRL_D 288   
#define danioKEY_CTRL_F 289   
#define danioKEY_CTRL_G 290   
#define danioKEY_CTRL_H 291   
#define danioKEY_CTRL_J 292   
#define danioKEY_CTRL_K 293   
#define danioKEY_CTRL_L 294   
#define danioKEY_CTRL_Z 300   
#define danioKEY_CTRL_X 301   
#define danioKEY_CTRL_C 302  // not caught by default
#define danioKEY_CTRL_V 303   
#define danioKEY_CTRL_B 304   
#define danioKEY_CTRL_N 305   
#define danioKEY_CTRL_M 306   

#define danioKEY_SHIFT_MINUS	586
#define danioKEY_SHIFT_PLUS	590   
// Key definitions    


/* old definitions from other danio keys.h

#define danioKEY_ESC 27 
#define danioKEY_F1 265
#define danioKEY_F2 266 
#define danioKEY_F3 267 
#define danioKEY_F4 268 
#define danioKEY_F5 269 
#define danioKEY_F6 270 
#define danioKEY_F7 271 
#define danioKEY_F8 272 
#define danioKEY_F9 273 
#define danioKEY_F10 274
#define danioKEY_F11 275 
#define danioKEY_F12 276 
	
//#define danioKEY_ENTER 13
#define danioKEY_ENTER 10 
		    
#define danioKEY_CTRL_A 1
#define danioKEY_CTRL_B 2 
#define danioKEY_CTRL_C 3 
#define danioKEY_CTRL_D 4 
#define danioKEY_CTRL_E 5 
#define danioKEY_CTRL_F 6 
#define danioKEY_CTRL_G 7 
#define danioKEY_CTRL_H 8 
#define danioKEY_CTRL_I 9 
#define danioKEY_CTRL_J 10 
#define danioKEY_CTRL_K 11 
#define danioKEY_CTRL_L 12 
#define danioKEY_CTRL_M 13 
#define danioKEY_CTRL_N 14 
#define danioKEY_CTRL_O 15
#define danioKEY_CTRL_P 16
#define danioKEY_CTRL_Q 17
#define danioKEY_CTRL_R 18 
#define danioKEY_CTRL_S 19 
#define danioKEY_CTRL_T 20 
#define danioKEY_CTRL_U 21 
#define danioKEY_CTRL_V 22 
#define danioKEY_CTRL_W 23 
#define danioKEY_CTRL_X 24 
#define danioKEY_CTRL_Y 25 
#define danioKEY_CTRL_Z 26 
		     
		     
#define danioKEY_DOWN_ARROW 258
#define danioKEY_UP_ARROW 259
#define danioKEY_LEFT_ARROW 260
#define danioKEY_RIGHT_ARROW 261
			    
#define danioKEY_PGDN 338
#define danioKEY_PGUP 339
#define danioKEY_END 360
#define danioKEY_HOME 262
#define danioKEY_INS 331
#define danioKEY_DEL 330
	
#define danioKEY_BACKSPACE 263
#define danioKEY_MINUS 45
#define danioKEY_PLUS 43
*/	
	
#endif // keys.h
