/*  dan_keyboard_buffer.h - buffered key input MY WAY
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

 			   
// use raw keyboard mode so that CTRL and ALT combinations can be recognised
// but use a software buffer to so we can deal in single key strokes.

// NOTE : not all key combinations have been defined, and key parssing is not
// complete. For any key stroke or combination intended to be used in a program
// first use dankeys to check how it is caught, then check it is covered by a 
// #define

#ifndef DAN_KEYBOARD_BUFFER_H
#define DAN_KEYBOARD_BUFFER_H

#include "/home/dan/source/include/dandefs.h"

#define FIRST_KEY 1 				     
#define LAST_KEY 111 				     
#define BUFF_SIZE 256 

// key code offsets				     
#define OFFSET_PRINTABLE 0
#define MAX_PRINTABLE    127
#define OFFSET_SCANCODE  128 			     
#define OFFSET_CTRL 	 256			     
#define OFFSET_ALT     	 384   			     
#define OFFSET_SHIFT   	 512   			     
#define OFFSET_CTRL_ALT	 640   			     

//#define ON 1	     // valid option or state  	       	       	
//#define OFF 0	     // valid option or state  	       	       	     
#define DISABLED -1  // state only  	       	       	     
		  
/* moved to keys.h
// key definitions
#define KEY_F1 187 	  
#define KEY_F2 188 
#define KEY_F3 189 	  
#define KEY_F4 190	  
#define KEY_F5 191  	  
#define KEY_F6 192   	  
#define KEY_F7 193   	  
#define KEY_F8 194   	  
#define KEY_F9 195   	  
#define KEY_F10 196  
#define KEY_F11 215   
#define KEY_F12 216   
		    
#define KEY_CTRL_F1  315	  
#define KEY_CTRL_F2  316	
#define KEY_CTRL_F3  317  	  
#define KEY_CTRL_F4  318 	  
#define KEY_CTRL_F5  319 	  
#define KEY_CTRL_F6  320  	  
#define KEY_CTRL_F7  321  	  
#define KEY_CTRL_F8  322  	  
#define KEY_CTRL_F9  323  	  
#define KEY_CTRL_F10 324  
#define KEY_CTRL_F11 343   
#define KEY_CTRL_F12 344   
	       	      	
#define KEY_UP_ARROW 231
#define KEY_DN_ARROW 236 
#define KEY_LF_ARROW 233  	 
#define KEY_RT_ARROW 234
	   	
#define KEY_CTRL_UP_ARROW 359
#define KEY_CTRL_DN_ARROW 364 
#define KEY_CTRL_LF_ARROW 361  	 
#define KEY_CTRL_RT_ARROW 362
		 	     
#define KEY_ALT_UP_ARROW 487 
#define KEY_ALT_DN_ARROW 364 
#define KEY_ALT_LF_ARROW 489   	 
#define KEY_ALT_RT_ARROW 490   

#define KEY_INS	  238   	
#define KEY_HOME  230   
#define KEY_PGUP  232   
#define KEY_DEL	  239   
#define KEY_END	  235   
#define KEY_PGDN  237  
		      
#define KEY_CTRL_INS  366   	
#define KEY_CTRL_HOME 358   
#define KEY_CTRL_PGUP 360   
#define KEY_CTRL_DEL  367   
#define KEY_CTRL_END  363 
#define KEY_CTRL_PGDN 365 
		     
#define KEY_MINUS       45	
#define KEY_PLUS        43	 
#define KEY_CTRL_MINUS 330	 
#define KEY_CTRL_PLUS  334	  
#define KEY_ALT_MINUS  458	
#define KEY_ALT_PLUS   462	  
		     
#define KEY_BACKSPACE 8	
#define KEY_ENTER 13 
#define KEY_ESCAPE 27

// CTRL_ALPHA - only use these if strict ascii is off
#define KEY_CTRL_Q 272   
#define KEY_CTRL_W 273
#define KEY_CTRL_E 274
#define KEY_CTRL_R 275   
#define KEY_CTRL_T 276   
#define KEY_CTRL_Y 277   
#define KEY_CTRL_U 278   
#define KEY_CTRL_I 279   
#define KEY_CTRL_O 280  
#define KEY_CTRL_P 281   
#define KEY_CTRL_A 286   
#define KEY_CTRL_S 287   
#define KEY_CTRL_D 288   
#define KEY_CTRL_F 289   
#define KEY_CTRL_G 290   
#define KEY_CTRL_H 291   
#define KEY_CTRL_J 292   
#define KEY_CTRL_K 293   
#define KEY_CTRL_L 294   
#define KEY_CTRL_Z 300   
#define KEY_CTRL_X 301   
#define KEY_CTRL_C 302   
#define KEY_CTRL_V 303   
#define KEY_CTRL_B 304   
#define KEY_CTRL_N 305   
#define KEY_CTRL_M 306   

#define KEY_SHIFT_MINUS	586
#define KEY_SHIFT_PLUS	590   
// Key definitions    
*/		      
		      
class dan_keyboard_buffer      	  		     
{     	       	    	       	  		     
public:  	    	       	  		     
  dan_keyboard_buffer(); 			     
  // initialisation constructor turns on keyboard raw mode
  int get_key();
  int wait_get_key();
  int update();	     				     
  // looks at the keyboard and updates the key log and the buffer 'keys'
  int push(int key); // adds the key to the buffer. returns zero on success
  int pull(); 	// gets the next key from the buffer. returns -1 if empty
  void set_capslock(int state = OFF) { capslock = state; }    	   	
  // set capslock ON, OFF or DISABLED		     
  // if DISABLED capslock does not affect other keys 
  void set_numlock(int state = OFF) { numlock = state; }	   	    
  // set numlock ON, OFF or DISABLED 		    
  // if DISABLED numlock does not affect other keys 
  void set_strict_ascii(int option = OFF) { strict_ascii = option; }
  // selectable option for remap
  // strict_ansi == 1 remaps CTRL with alpha characters (and some others) 
  // into the ascii range. eg CTRL-H becomes backspace, CTRL-M is carrage 
  // return. Set to 0 to map these keys beyond the ascii range.
  // set ON or OFF 
  void set_condense_map(int option = OFF) { condense_map = option; }
  // selectable option for remap
  // condense_map == 1 maps similarly labled keys to the same position,
  // eg return and enter or the numeric keypad keys (dependent on the state of 
  // numlock
  // set ON or OFF 
      
private:	  		  				   
  int remap(int key); 		  	
  // converts scancode information to codes for the key buffer
  char *key_state; 
  // the state of the keyboard as at the last update() 
  // uses keyboard_update() and get_state() from vgakeyboard.h 
  char key_log[LAST_KEY]; 
  // a log of what keys were held down at the previous update 
  // (initialised to none) if a key is down in the key_log and then found to
  // be released by a subsequent update it is counted as a key press and the
  // key stroke is placed in the buffer 'keys' after the SCANCODE has been
  // passed by remap()
  int keys[BUFF_SIZE]; // a buffer for recorded keystrokes	  
  int *last_key;       // last key is used to mark the most recent key	       
  // stroke avilable in the buffer 'keys'. when keys contains one or more
  // key strokes, last key points to a keystroke within keys. When keys
  // is empty, last key points to an adress just before keys. This address
  // must not be read or written to as it is not within the programs memmory
  // space.    	       				      
  int *end;   // the last position in keys		
  int ctrl, alt, shift;	// the state of the ctrl, alt and shift keys 
  // (KEYPRESSED or KEYNOTPRESSED)  
  int capslock, numlock; // toggled states that alter other keys
  int strict_ascii, condense_map; // see set_strict_ascii() 
                                  // and set_condense_map()
}; // dan_keyboard_buffer    	       	      	    		  
      		      	    
      		      
#endif  	      	    
// end - dan_keyboard_buffer.h
