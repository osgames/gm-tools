#includable makefile - for danio 
#Daniel Vale MAY 2002

ifndef DANIO_MK
DANIO_MK = 1

ifeq ($(OS),Windows_NT)
DANIO_ALL := $(DANIO_ALL) $(DANIO_DIR)/conio.o
endif

$(DANIO_DIR)/../danio.o : $(DANIO_ALL)
	g++ $(DIR_LIST) -c $(DANIO_DIR)/danio.cc -o $(DANIO_DIR)/../danio.o

$(DANIO_DIR)/../danscr.o : $(DANIO_DIR)/danscr.cc $(DANIO_DIR)/../danscr.h
	g++ $(DIR_LIST) -c $(DANIO_DIR)/danscr.cc -o $(DANIO_DIR)/../danscr.o

$(DANIO_DIR)/../dansys.o : $(DANIO_DIR)/dansys.cc $(DANIO_DIR)/../dansys.h
	g++ $(DIR_LIST) -c $(DANIO_DIR)/dansys.cc -o $(DANIO_DIR)/../dansys.o

$(DANIO_DIR)/../dwindow.o : $(DANIO_DIR)/dwindow.cc $(DANIO_DIR)/../dwindow.h
	g++ $(DIR_LIST) -c $(DANIO_DIR)/dwindow.cc -o $(DANIO_DIR)/../dwindow.o

$(DANIO_DIR)/../dankbd.o : $(DANIO_DIR)/dankbd.cc $(DANIO_DIR)/../dankbd.h
	g++ $(DIR_LIST) -c $(DANIO_DIR)/dankbd.cc -o $(DANIO_DIR)/../dankbd.o

# Extra object files required because they are not in libries?
# I would like to have put this in the implementation specific link.mk for conio
# but that makes it the build target. 
# I need to learn how to build libries I think. FIX ME

ifeq ($(OS),Windows_NT)
$(DANIO_DIR)/conio.o : $(DANIO_DIR)/conio.h $(DANIO_DIR)/conio.c
	g++ -c $(DANIO_DIR)/conio.c -o $(DANIO_DIR)/conio.o
endif

endif
