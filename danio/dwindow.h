/*  dwindow.h - windowing encapsulation for danio project
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */

// This is the interface for all objects that display themselves in a window.

#ifndef DWINDOW_H
#define DWINDOW_H

#include <stdarg.h>

// window stuff
class dwindow 
// to provide cross platform windowing, must encapsulate all window 
// attributes in a cross platform class			  
// NOTE: the top left corner of the screen is 1,1
{            				    	  
public:					    	  
  dwindow(int new_x1, int new_y1, int new_x2, int new_y2, 
	  char *new_name = NULL);
    // window constructor		       
  
  void setscroll(int scrl);
    // turn scolling on or off
    // use of constants ON and OFF from dandefs.h is recommended
  
  void dwrite(char *s, ...);
    // formatted output to a window

  void dvwrite(char *s, va_list ap);
    // formatted output to a window using a processed argument list

  void dcur_move(int new_x, int new_y);
    // move the cursor

  void get_dcur_xy(int& x, int& y);
    // find the position of the cursor

  int get_dcur_x();
    // find the column

  int get_dcur_y();
    // find the row

  void dclear();
    // clear the window

  void drefresh();
    // redraw the screen and flush any buffered screen printing

  int setpcolour(int new_colour);
    // change the printing colour
    


  protected: 
  void *get_win_ptr() { return win_ptr; }      	    
    // return the pointer to a third party window   

  int x1, y1, x2, y2;  // top left and bottom right corners of the window
  int wwidth, wheight; // size of the window 
  //  int cur_x, cur_y; // for tracking the writing cursor in the window FIXME: these are not maintained, use get_dcur_*
  //  int draw_x, draw_y; // for tracking the drawing cursor in the window 
  void *win_ptr; // pointer to a third party window - implementation / platform specific
  char *name; // used to give a window a name, usefull for debugging
  int scrl; 

}; // dwindow

#endif

// end - dwindow.h
