/* danio.h - header for my io routines 
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
// see SOURCE_DIR/doc/DANIO for more information about the uses of this code.

// WARNING: dankbd.h may not work without danscr. in the case of ncurses it 
// does not

#ifndef DANIO_H	
#define DANIO_H		   

// These files contain various functions an application may wish to call.
#include "dankbd.h" // keyboard io
#include "danscr.h" // screan io
#include "dwindow.h" // window based interface
#include "keys.h"  // Contains mnemonic definitions of keys. Find this in 
                   // the appropriate interface directory. Actual values 
                   // vary with the interface.
#include "pallet.h" // colour definitions
#include "dansys.h" // non io system dependant functions

// These functions initialise and terminate the interface.
int init_danio();
  // initialise appropriate interface
  // return 0 on success, anything else on failure
void close_danio();
  // terminate a dependant interface

#endif		   
// end - danio.h
				
