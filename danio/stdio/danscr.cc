/*  danscr.cc - danio screen wrappers
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
       	     
// stdio.h version - implements danscr with stdio.h
// This implementation produces run of the mill console output
 	   	 
#include <stdio.h>
#include <stdarg.h>
#include "../danscr.h"
#include "/home/dan/source/include/sat.h"

int init_danscr()	       
// stub - nothing to initialise
{  	       
  return 0;    
}	       
	       
void dwrite(char *fmt, ...)
{ 	       	 	  
  va_list ap;   	     
  char *s;     	     
      	       	       	     
  va_start(ap, fmt);
  vprintf(fmt, ap);   	       	     
  va_end(ap);              	  
	       
} // dwrite    	     	 

void dwrite(dwindow *win, char *fmt, ...)
// windows are ignored by stdio implementation	    
{ 	       	 	  
  va_list ap;   	     
  char *s;     	     
      	       	       	     
  va_start(ap, fmt);
  vprintf(fmt, ap);   	       	     
  va_end(ap);              	  
	       
} // dwritew

void dvwrite(char *s, va_list ap)
// formated output, using a va_list previously extracted
{ 
  vprintf(s, ap);
}// dvwrite	    


void dclear()
{   
  printf("\n");
}   	       

void dclear(dwindow *win)
{ 
  printf("\n");
}

void drefresh(dwindow *win)
{	       
  // do nothing
}	       
	       
	       
void close_danscr()
// stub - nothing to close
{	       	 	  
}	       
	       
void dcur_move(int x, int y)
  // move the writing cursor to column x, row y
{	       
  // can't be done ???
} // dan cursor move
	       
// pallet stuff - don't have colour, these are just stubs so it will compile
int setpcolour(short new_colour) 
{	       	 
  return 1;
} 

int wsetpcolour(dwindow *win, short new_colour)
{ 	   
  return 1;
} 
  
//                          WINDOW
// don't have this either  

dwindow::dwindow(int new_x1, int new_y1, int new_x2, int new_y2, char *new_name)
{  	       	       	       				       
  x1 = new_x1; 
  y1 = new_y1;
  x2 = new_x2; 
  y2 = new_y2;
  win_ptr = NULL;
  name = new_str(new_name);
}			   
			   
