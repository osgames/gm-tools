/*  dankbd.cc - danio keyboard wrappers
 * 
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires see http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */
    
// stdio.h version - implements dankbd.h with stdio.h
 	   	 					
#include <stdio.h>
#include <stdarg.h>	
//#ifndef _IONBF		    
//#define _IONBF 2
//#endif	     		    
  	     		    
int init_dankbd()      	       
{ 	     	       	    
  setbuf(stdin, NULL); // doesn't seem to work, 
  // line buffering of input happens anyway ??? tk
  // need some kind of work around ?
  return 0;    
}	       
	       
int dgetch()  	 
{	       	 
  return getchar();
}//dgetch      	 
	       	 
void close_dankbd()
// stub - nothing to close
{	   	 	  
}		 
		 
		 
