/* status_line.cc - status line display
 *
 * C Free Software Foundation 2004
 *
 * This file is part of the GM Tools project.
 * The GM Tools are free software; you can redistribute them and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GM Tools are distributed in the hope that they will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the GM Tools; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For general enquires about GM Tools see
 * http://sourceforge.net/projects/gm-tools
 * or send e-mail to dan_vale@users.sourceforge.net
 *
 */


#include <string.h>
#include <stdarg.h>

#include "status_line.h"
#include "sat.h"
#include "danio.h"
#include "dandefs.h"
#include "err_log.h"

void text_norm(dwindow *win)
{
  setpcolour(COLOUR_NORM, win);
}

void text_highlight(dwindow *win)
{
  setpcolour(COLOUR_HIGHLIGHT, win);
}


void clear_status_line()
// blank the status line in the appropriate background colour
{
  log_event("clear_status_line()\n");
  setpcolour(STATUS_COLOUR);
  dcur_move(1, STATUS_LINE);
  dwrite(STATUS_BLANK);
} // clear status line

void say_status(char *msg, ...)
// say something on the status line
{
  va_list ap;
  int x;

  va_start(ap, msg);

  // display the message
  setpcolour(STATUS_COLOUR);
  dcur_move(1, STATUS_LINE);
  dvwrite(msg, ap);
  x = get_dcur_x();
  log_event("say_status() - ending at x-ordinate: %d\n  Message follows: ", x);
  log_event(msg, ap); log_event("\n");
  for (; x < TIME_X; x++)
    dwrite(" "); // ??? tk this is terrably inefficient

  va_end(ap);
} // say status

void clear_prompt()
{
  dcur_move(1, PROMPT_LINE);
  dcleareol();
} // clear prompt

void say_prompt(char *msg, ...)
{
  int x;
  va_list ap;

  va_start(ap, msg);
  setpcolour(PROMPT_COLOUR);
  dcur_move(1, PROMPT_LINE);
  dwrite(msg, ap);
  va_end(ap);
  dcleareol();
  dcur_move(strlen(msg)+1, PROMPT_LINE);
} // say prompt


void bad_input()
{
  say_status("You're entry was not understood.");
} // bad input

// end - status_line.cc


