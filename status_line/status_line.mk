#include(able) makefile for status_line.o
#Daniel Vale MAY 2002

ifndef STATUS_LINE_MK
STATUS_LINE_MK := 1

include $(SOURCE_DIR)/include/map.mk

$(STATUS_DIR)/status_line.o : $(STATUS_DIR)/status_line.cc \
  $(STATUS_DIR)/status_line.h $(DANIO_H) $(INCLUDE_DIR)/sat.h
	g++ -c -o $(STATUS_DIR)/status_line.o $(DIR_LIST) $(STATUS_DIR)/status_line.cc

endif

